package dope.dopeclient;
import java.util.Comparator;
import dope.dopeclient.CacheEntry;
import java.util.List;

/**
 * Class for comparing two cache entries by the paths of their encodings.
 * If entry1 is to the left of entry2 in the binary encoding tree then
 * entry1 is less than entry2.
 */
public class CacheEntryComparator implements Comparator<CacheEntry>{
    @Override
    public int compare(CacheEntry ce1, CacheEntry ce2) {
        List<Integer> enc1 = ce1.encoding;
        int len1 = ce1.encoding.size();
        List<Integer> enc2 = ce2.encoding;
        int len2 = ce2.encoding.size();

        int min_len = java.lang.Math.min(len1,len2);
        for (int i = 0; i < min_len; i++){
            int elt1 = enc1.get(i);
            int elt2 = enc2.get(i);

            /* elt1 branches right elt2 branches left */
            if (elt1 == 1 && elt2 == 0){
                return 1;
            }

            /* elt1 branches left elt2 branches right */
            if (elt1 == 0 && elt2 == 1){
                return -1;
            }

            /* Equal at this level continue check */
        }
        if (len1 > len2){
            if (enc1.get(min_len) == 1){
                return 1;
            }
            else {
                return -1;
            }
        }
        else if (len2 > len1){
            if (enc2.get(min_len) == 1){
                return -1;
            }
            else {
                return 1;
            }
        }
        else {
            return 0;
        }

    }
}
