package dope.dopeclient;
import dope.dopeclient.CacheEntry;
import dope.dopeclient.CacheEntryComparator;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Collections;

public class CacheModel {
    int max_size;
    int current_size;
    List<CacheEntry> cache;
    int lru_tag;
    double sg_alpha;

    public CacheModel(int table_size) {
        this.max_size = table_size;
        this.current_size = 0;
        this.cache= new ArrayList<CacheEntry>();
        this.lru_tag = 0;
        this.sg_alpha = 0.5;
    }

    /**
     * Return true if compare is an encoding that belongs in the subtree
     * rooted at encoding root
     */
    private boolean enc_in_subtree(List<Integer> root, int start_level,
                                   List<Integer> compare){
        List<Integer> up_to_start = new ArrayList<Integer>(compare.subList(0,
                start_level));
        return up_to_start.equals(root);
    }


    /**
     * Rebalance the subtree rooted at the index.  Precondition:
     * the subtree is in the cache.
     */
    private void rebalance_node(CacheEntry entry) {
        List<Integer> start_encoding = new ArrayList<Integer>(entry.get_encoding());
        int start_level = start_encoding.size();
        /* Create a list containing all the entries in cache
           that belong to the subtree to be rebalanced */
        List<CacheEntry> subtree_list = new ArrayList<CacheEntry>(cache);
        for (Iterator<CacheEntry> iterator = subtree_list.iterator();
             iterator.hasNext();){
            if (!enc_in_subtree(start_encoding, start_level,
                    iterator.next().encoding)) {
                iterator.remove();
            }
        }
        /* Clear the cache of the changing subtree */
        for (Iterator<CacheEntry> iterator = this.cache.iterator();
             iterator.hasNext();){
            if (enc_in_subtree(start_encoding, start_level,
                    iterator.next().encoding)) {
                iterator.remove();
            }
        }

        /* Sort the cache entries by encoding */
        Collections.sort(subtree_list, new CacheEntryComparator());
        List<CacheEntry> rebalanced_subtree = build_balanced(start_encoding,
                subtree_list);
        merge(rebalanced_subtree);
    }

    /**
     * Return a balanced subtree cache entry list, as determined by
     * reassigning encodings, provided with an ordered encoding list
     */
    private List<CacheEntry> build_balanced(List<Integer> start_encoding,
                                            List<CacheEntry> subtree_list){
        /* Base case */
        if (subtree_list.isEmpty()) {
            return new ArrayList<CacheEntry>();
        }
        int mid_index = subtree_list.size()/2; //Expecting truncating division
        List<CacheEntry> left = new ArrayList<CacheEntry>
                                    (subtree_list.subList(0, mid_index));
        List<CacheEntry> right = new ArrayList<CacheEntry>
                                     (subtree_list.subList(mid_index + 1,
                                             subtree_list.size()));
        CacheEntry oldMiddle = subtree_list.get(mid_index);
        List<Integer> encoding = new ArrayList<Integer>(start_encoding);
        CacheEntry entry = new CacheEntry(oldMiddle.cipher_text, encoding);

        List<Integer> r_enc = new ArrayList<Integer>(start_encoding);
        r_enc.add(1);
        List<Integer> l_enc = new ArrayList<Integer>(start_encoding);
        l_enc.add(0);
        List<CacheEntry> r_entries = build_balanced(r_enc, right);
        List<CacheEntry> l_entries = build_balanced(l_enc, left);


        /* Update entry metadata */
        entry.subtree_size = 1 + r_entries.size() + l_entries.size();
        if (r_entries.isEmpty() || l_entries.isEmpty()){
            entry.is_leaf = false;
        }
        if (r_entries.size() != 0){
            entry.has_right_child = true;
        }
        if (l_entries.size() != 0){
            entry.has_left_child = true;
        }

        l_entries.add(entry);
        l_entries.addAll(r_entries);
        return l_entries;

    }

    /*
     * Return the entry with the provided encoding in the cache.  If no such
     * entry exists then return null
     */
    public CacheEntry entry_with_encoding(List<Integer> encoding) {
        for (Iterator<CacheEntry> iterator = this.cache.iterator();
             iterator.hasNext();){
            CacheEntry nextEntry = iterator.next();
            if (nextEntry.encoding.equals(encoding)){
                return nextEntry;
            }
        }
        return null;
    }

    /*
     * Go through all entries affected by adding the entry with
     * encoding 'child_enc', and update their subtree sizes and
     * child flags.
     */
    private void update_parent_sizes(List<Integer> child_enc) {
        int level = 0;
        while (level < child_enc.size()){
            List<Integer> level_enc = new ArrayList<Integer>
                    (child_enc.subList(0,level));
            CacheEntry parent = entry_with_encoding(level_enc);
            if (parent != null){
                parent.subtree_size++;
                /* Child is greater than parent */
                if (child_enc.get(level) == 1){
                    parent.has_right_child = true;
                }
                /* Child is less than parent */
                if (child_enc.get(level) == 0){
                    parent.has_left_child = true;
                }
            }
            level++;
        }
    }


    /**
     * Merge new entries into the existing cache. Used for internal reordering
     * so current size is not affected by this operation
     */
    public void merge(List<CacheEntry> incoming_entries) {
        /* Filter out all entries already in cache */
        incoming_entries.removeAll(this.cache);
        this.cache.addAll(incoming_entries);
    }

    /**
     * Filter incoming entries, only merging in new entries updating
     * subtree sizes to match tree in lower hierarchy.  Precondition:
     * entry has not been added to the cache yet.  This must hold to
     * guarantee no overcounting of subtree sizes on multiple inserts.
     * This method takes in syncs from tier below, while merge is used
     * on insert requests from above or internal rebalance merges.
     */
    public void merge_new(List<CacheEntry> incoming_entries) {
        /* Filter out all entries already in cache */
        incoming_entries.removeAll(this.cache);
        /* Add to cache */
        this.cache.addAll(incoming_entries);
        this.current_size++;

        /* Update parent subtree size and child ownership flags */
        for (Iterator<CacheEntry> iterator = incoming_entries.iterator();
             iterator.hasNext();){
            update_parent_sizes(iterator.next().encoding);
        }

        /* Handle evictions (Later for sized caches when there is cloud) */
    }

}
