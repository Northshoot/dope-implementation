package dope.dopeclient;
import java.util.List;
import java.nio.ByteBuffer;

/**
 * An entry in the dOPE cache.  Contains the ciphertext, encoding,
 * and metadata used during inserts at the sensor.
 */
public class CacheEntry {
    byte[] cipher_text;
    short subtree_size;
    boolean is_leaf;
    boolean has_left_child;
    boolean has_right_child;
    boolean synced;
    int lru;
    List<Integer> encoding;


    /* Constructor for making a new cache entry from data transferred
     from the sensor over bluetooth.  Sync cipher has 16 bytes of data,
     sync meta has 8 bytes of data. */
    public CacheEntry(byte[] sync_cipher, byte[] sync_meta){
        this.synced = false;
        this.lru = 0; //LRU meaningless on highest hierarchy
                      //This will change upon introducing cloud
        System.arraycopy(sync_cipher, 0, this.cipher_text, 0, 16);
        ByteBuffer bb = ByteBuffer.wrap(sync_meta);
        /* Get 4 bytes of encoding body */
        //int enc_body = bb.get
        /* Get byte of encoding length */
        int enc_len = sync_meta[4] & 0xFF;
        /* Make encoding list */

        /* Get subtree size short */

        /* Get byte of flags */

        /* Parse flags */

        return;
    }

    /* Constructor for copying over entries during rebalance.  Also
       useful for testing */
    public CacheEntry(byte[] cipher, List<Integer> encoding){
        this.cipher_text = cipher;
        this.encoding = encoding;
    }
    public List<Integer> get_encoding(){
        return this.encoding;
    }

    /*
     * Position in encoding tree should be unique, so compare by encoding
     */
    @Override
    public boolean equals(Object obj){
        if (!(obj instanceof CacheEntry))
            return false;
        if (obj == this)
            return true;
        CacheEntry ce = (CacheEntry) obj;
        return this.encoding.equals(ce.encoding);
    }
}
