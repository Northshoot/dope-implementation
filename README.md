# Implementation of the dOPE spanning three tiers of IoT

This is project for the implementation of the dOPE spanning three tiers of the IoT.
The implementation consist of a pattern e.g. hand coded device/gateway/cloud applications and the ravel application which generates all the necessary code.

## folder structure

- android contains the android code of dOPE
- cloud contains the django code of dOPE
- nrf52 contains the embedded nrf52 code of dOPE
- ravel contains ravel app with the dOPE

## Branch structure
- dev - current developing version for all
- ravel - ravel implementation
- pattern - branch for holistic work on embedded/android/cloud
- wyatt/larry - individual dev branches

## wiki
Wiki contains documentation and specifications of BLE services for the dOPE, architecture description etc
