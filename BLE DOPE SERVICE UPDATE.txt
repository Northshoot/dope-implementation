BLE DOPE SERVICE

Base UUID value: 0x0x35f9038da0bd4fff90318a4e00007758
dOPE Service UUID descriptor: 0x0001

Characteristics                      
_______________

Sync Meta Characteristic:
   Characteristic UUID descriptor: 0x0002

--Used to send dOPE metadata
--Value created at the sensor
--Value contains cache meta data (encoding, subtree size, flags)
--Length = 8 bytes
--Communicated to client by INDICATE 
--contains a cccd for client to turn on indication

Sync Cipher Characteristic:
   Characteristic UUID descriptor: 0x0003

--Used to send dOPE ciphertext
--Value created at the sensor
--Value contains cache deterministic encrypted data (AES ECB)
--Length = 16 bytes
--Communicated to client by INDICATE
--contains a cccd for client to turn on indication

Control Message Characteristic:
   Characteristic UUID descriptor: 0x0004
   
   Control Message enum type constants:
   BLE_DOPS_CTRL_REBAL 0
   BLE_DOPS_CTRL_MISS  1

--Used to send gateway information about changes in dOPE state,
  specifically, information about sensor rebalance requests and 
  cache misses
--Value generated by the sensor
--Value contains REBAL or MISS control message, an encoding indicating
  either the missing encoding to return or the root of the tree to
  be rebalanced, and a bit indicating if this is the final message
  for this event (relevant only for rebalance events)
--Length = 6 bytes
--Communicated to client by INDICATE
--contains a cccd for a client to turn on indication

Miss Meta Characteristic:
   Characteristic UUID descriptor: 0x0005

   --Used to communicate meta-data to handle cache miss to sensor
   --Value looked up at gateway and written to sensor
   --Value contains cache metadata of missing entry
   --Length = 8 bytes
   --Guaranteed to be writeable at the sensor's gatt server

Miss Cipher Characteristic:
  Characteristic UUID descriptor: 0x0006

  --Used to communicate data to handle cache miss to sensor
  --Value looked up at gateway and written to sensor
  --Value contains ciphertext of missing entry (AES ECB)
  --Length = 16 bytes
  --Guaranteed to be writeable at the sensor's gatt server

Rebalance Response Characteristic:
   Characteristic UUID descriptor: 0x0007

   --Used to communicate that gateway has finished rebalance
   --Value written to sensor from gateway client
   --Value contains a single boolean set high by gateway
   --Length = 1 bytes
   --Guaranteed to be writeable at the sensor's gatt server


Protocol Organization
_____________________

1. The sensor's server and the gateway's client begin by connecting.
   In this step the embedded device's indicating characteristics'
   cccd's are written as active by the gateway client, and the client
   discover's the embedded device.  Two way communication is now possible

2. The next datum arrives from the sensor, it is enqueued in a buffer
   or dropped if there is no room in the buffer.

3. If the dOPE cache is in a state that can accept new data, dOPE inserts 
   the next datum into the cache.

4. After a normal (no rebals or misses) cache insert, new data is added
   to the sync queue.  The next outgoing sync is then written to the 
   dOPE service's sync characteristic, and then indicated to the gateway.
   The dOPE cache now blocks further inserts.  First the metadata is 
   indicated, after receiving an acknowledgement the ciphertext is
   indicated. If a miss or a rebalance occurs during insertion, go to (6.) (8.).

5. Upon reading the embedded device's sync characteristic and completing the
   indication, the gateway resets the cache to allow inserts and 
   triggers the embedded device to continue inserting data into the cache. 
   Go to (3.)

6. MISS: If a cache miss occurs during insertion a control message is indicated
   to the gateway, asking for the missing ciphertext with a given encoding.
   The dOPE cache blocks and will only resume after the dOPE service's 
   miss response characteristic is written by the gateway client.  Like the
   sync, the miss is broken into metadata and ciphertext, so the client must
   make two ordered writes, first the metadata, then the ciphertext.

7. The gateway client writes the missing ciphertext, the missing entry merged, and
   the dOPE cache is set back to a writeable state.  The interrupted insert is
   continued.  Upon finishing go to (4.)

8. REBAL: The sensor cache requests a rebalance.  It prioritizes all unsynced entries
   in the part of the cache to be flushed.  A control message is indicated to the
   gateway client.

9. The client receives the rebalance control message by indication and then
   writes the rebal response.  If any cache entries remain to be synced 
   before rebalance can occur, then the embedded device sends a sync message
   and decrements its count of remaining sync messages.  Upon indication of 
   sync messages while in a rebalance state, the embedded device continues
   to flush its sync queues as needed, by continuing to indicate the 
   necessary syncs.

10. After all sync queues have been flushed a final rebalance control message
    is sent, indicating that the rebalance needs no more information from the 
    embedded device.  The dOPE cache is blocked to all inserts while waiting
    for rebalance to complete.

11. Gateway client writes the rebalance response message characteristic's bit
    high to indicate that it has finished rebalances.  The embedded device 
    resets this bit and restores the cache to an insertable state.  The next
    enqueued datum is popped and inserted into the dOPE cache.  When finished
    insert go to (4.)







