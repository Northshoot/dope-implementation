Wyatt Daviau --- Feb 2016

This directory contains the Nordic BLE SDK, with dOPE and mOPE services 
added in.  Two places contain the chages relevant to dOPE.  
nRF52_SDK_0.9.2_dbc28c9/components/ble/ble_services/ble_dope contains the 
implementation of ble mOPE and dOPE service. 

nRF52_SDK_0.9.2_dbc28c9/examples/ble_peripheral/ble_app_dope contains the
current testing application which simply inserts data into the dOPE service.