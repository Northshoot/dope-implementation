#include "ble_mope.h"
#include "nordic_common.h"
#include "ble_l2cap.h"
#include "ble_srv_common.h"
#include "app_util.h"
#include "mem_manager.h"

static unsigned char mope_uuid_base_val[16] = {0x3d, 0xf9, 0x03, 0x8d, 0xa0, 0xbd, 0x4f, 0xff, 0x90, 0x31, 0x8a, 0x4e, 0x00, 0x00, 0x77, 0x58};

/* Function for handling the Connect event */
static void on_connect(ble_mops_t *p_mops, ble_evt_t *p_ble_evt)
{
    p_mops->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
}

/* Function for handling the Disconnect event */
static void on_disconnect(ble_mops_t *p_mops, ble_evt_t *p_ble_evt)
{
    UNUSED_PARAMETER(p_ble_evt);
    p_mops->conn_handle = BLE_CONN_HANDLE_INVALID
}

/* Function for handling write events to the cipher characteristic's cccd */
static void on_cipher_cccd_write(ble_mops_t *p_mops, ble_gatts_evt_write_t
                                 *p_evt_write)
{
    if (p_evt_write->len == 2)
    {
        //CCD written, update indication state
        if (p_mops->evt_handler != NULL)
        {
            ble_mops_evt_t evt;
            if (ble_srv_is_indication_enabled(p_evt_write->data))
            {
                evt.evt_type = BLE_MOPS_EVT_CIPHER_INDICATION_ENABLED;

            }
            else
            {
                evt.evt_type = BLE_MOPS_EVT_CIPHER_INDICATION_DISABLED;
            }
        }
    }
}

/* Function for handling write events to the MtoG characteristic's cccd */
static void on_MtoG_cccd_write(ble_mops_t *p_mops, ble_gatts_evt_write_t
                               *p_evt_write)
{
    if (p_evt_write->len == 2)
    {
        //CCCD written update indication state
        if (p_mops->evt_handler != NULL)
        {
            ble_mops_evt_t evt;
            if (ble_srv_is_indication_enabled(p_evt_write->data))
            {
                evt.evt_type = BLE_MOPS_EVT_RESPONSE_INDICATION_ENABLED;
            }
            else
            {
                evt.evt_type = BLE_MOPS_EVT_RESPONSE_INDICATION_DISABLED;
            }
        }
    }
} 

/* Function for responding to Gateway request to compare two ciphertexts */
static void on_comparison_request(ble_mops_t *p_mops, ble_gatts_evt_write_t
                                  *p_evt_write)
{
    // Read the two ciphers, decrypt and compare
    uint32_t err_code;
    uint8_t[16] p1;
    uint8_t[16] p2;
    bool crypt_success;

    crypt_success = nrf_ecb_crypt(p1, p_mops->GtoM.c1);
    if (!crypt_success) 
    {
        return ERROR_INTERNAL;
    }
    nrf_ecb_crypt(p2, p_mops->GtoM.c2);
    if (!crypt_success)
    {
        return ERROR_INTERNAL;
    }

    uint16_t d1;
    uint16_t d2;

    // Set the value of the response based on ordering of plaintexts
    memcpy(&d1, p1, sizeof(uint16_t));
    memcpy(&d2, p2, sizeof(uint16_t));

    p->mops->MtoG = p1 > p2;

    // Start up indication messages (send GtoM)
    if (p_mops->conn_handle != BLE_CONN_HANDLE_INVALID)
    {
        uint16_t hvx_len;
        ble_gatts_hvx_params_t hvx_params;

        hvx_len = sizeof(int);

        memset(&hvx_params, 0, sizeof(hvx_params));

        hvx_params.handle = p_mops->GtoM_handls.value_handle;
        hvx_params.type   = BLE_GATT_HVX_INDICATION;
        hvx_params.offset = 0;
        hvx_params.p_len  = &hvx_len;
        hvx_params.p_data = &p_mops->MtoG;

        err_code = sd_ble_gatts_hvx(p_mops->conn_handle, &hvx_params);
        if ((err_code == NRF_SUCCESS) && (hvx_len != sizeof(int)) )
        {
            err_code = NRF_ERROR_DATA_SIZE;
        }
    }
    else
    {
        err_code = NRF_ERROR_INVALID_STATE;
    }
    return err_code;
}

/* Function for receiving 'insertion complete' message and sending anything
   still on the queue. */
static void on_insert_complete(ble_mops_t *p_mops, ble_gatts_evt_write_t
                               *p_evt_write)
{
    // Check that gateway wrote complete, if not return
    if (p_mops->insert_success != 1) {
        return;
    }
    p_mops->insert_success = 0;

    // If anymore data enqueued go ahead and start sending
    uint32_t err_code;
    size_t len = sizeof(uint16_t);
    uint16_t data;
    err_code = app_fifo_get_len(&p_mops->input_buffer, &data, &len);
    if (err_code != NRF_SUCCESS) 
    {
        p_mops->blocking = 0;
        return; // Nothing in the buffer
    }

    // More data to send
    err_code = ble_mops_send_data(p_mops, data);
    if (err_code != NRF_SUCCESS) {
        p_mops->blocking = 0;
    }
    return err_code;
}

/* Function for handling the Write event */
static void on_write(ble_mops_t *p_mops, ble_evt_t *p_ble_evt)
{
    ble_gatts_evt_write_t *p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;
    if (p_evt_write->handle == p_mops->cipher_handles.cccd_handle)
    {
        on_cipher_cccd_write(p_mops, p_evt_write);
    } else if (p_evt_write->handle == p_mops->MtoG_handles.cccd_handle)
    {
        on_MtoG_cccd_write(p_mops, p_evt_write);
    } else if (p_evt_write->handle == p_mops->GtoM_handles.value_handle)
    {
        on_comparison_request(p_mops, p_evt_write);
    } else if (p_evt_write->handle == p_mops->insert_success_handles.value_handle)
    {
        on_insert_complete(p_mops, p_evt_write);
    }
}

/* Handle ble events */
void ble_mops_on_ble_evt(ble_mops_t *p_mops, ble_evt_t *p_ble_evt)
{
    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
          on_connect(p_mops, p_ble_evt);
          break;
        case BLE_GAP_EVT_DISCONNECTED;
          on_disconnect(p_mops, p_ble_evt);
          break;
        case BLE_GATTS_EVT_WRITE:
          on_write(p_mops, p_ble_evt);
          break
    }
}

/* Add the cipher characteristic */
static uint32_t cipher_char_add(ble_mops_t *p_mops, const ble_mops_init_t *p_mope_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_md_t cccd_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;
    cipher_t*           initial_cipher;

    // Init cccd
    memset(&cccd_md, 0, sizeof(cccd_md));
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    cccd_md.write_perm = p_mope_init->mops_cipher_attr_md.cccd_write_perm;
    cccd_md.vloc       = BLE_GATTS_VLOC_STACK;

    memset(&char_md, 0, sizeof(char_md));

    char_md.char_props.read     = 1;
    char_md.char_props.indicate = 1;
    char_md.p_char_pf           = NULL;
    char_md.p_user_desc_md      = NULL;
    char_md.p_cccd_md           = &cccd_md;
    char_md.p_sccd_md           = NULL;

    ble_uuid.type = p_mops->uuid_type;
    ble_uuid.uuid = BLE_UUID_CIPHER_CHAR;

    attr_md.read_perm  = p_mope_init->mops_cipher_attr_md.read_perm;
    attr_md.write_perm = p_mope_init->mops_cipher_attr_md.write_perm;
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 1;

    initial_cipher = p_mops->cipher;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = sizeof(cipher_t);
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = sizeof(cipher_t);
    attr_char_value.p_value   = initial_cipher;

    return sd_ble_gatts_characteristic_add(p_mops->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_mops->cipher_handles);

}

/* Add the Gateway to embedded message characteristic */
static uint32_t GtoM_char_add(ble_mops_t *p_mops, const ble_mops_init_t *p_mope_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuit_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;

    memset(&char_md, 0, sizeof(char_md));

    char_md.char_props.read     = 1;
    char_md.char_props.indicate = 1
    char_md.p_char_user_desc    = NULL;
    char_md.p_char_pf           = NULL;
    char_md.p_user_desc_md      = NULL;
    char_md.p_cccd_md           = NULL;
    char_md.sccd_md             = NULL;

    ble_uuid.type = p_mops->uuid_type;
    ble_uuid.uuid = BLE_UUID_GATETOEMBED_CHAR;

    attr_md.read_perm  = p_mope_init->mops_GtoM_attr_md.read_perm;
    attr_md.write_perm = p_mope_init->mops_GtoM_attr_md.write_perm;
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 1;

    initial_GtoM = &p_mops->GtoM;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = sizeof(GtoM_t);
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = sizeof(GtoM_t);
    attr_char_value.p_value   = initial_GtoM;

    return sd_ble_gatts_characteristic_add(p_mops->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_mops->GtoM_handles)

}

/* Add the Embedded to gateway message characteristic */
static uint32_t MtoG_char_add(ble_mops_t *p_mops, const ble_mops_init_t *p_mope_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_md_t cccd_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;
    int                 initial_MtoG;

    memset(&cccd_md, 0, sizeof(cccd_md));
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    cccd_md.write_perm = p_mope_init->mops_MtoG_attr_md.cccd_write_perm;
    cccd_md.vloc       = BLE_GATTS_VLOC_STACK;

    memset(&char_md, 0, sizeof(char_md))

    char_md.char_props.read   = 1;
    char_md.char_props.indicate = 1;
    char_md.p_char_user_desc  = desc;
    char_md.p_char_pf         = NULL;
    char_md.char_user_desc_max_size = 4;
    char_md.char_user_desc_size = 4;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = &cccd_md;
    char_md.p_sccd_md         = NULL;

    ble_uuid.type = p_mops->uuid_type;
    ble_uuid.uuid = BLE_UUID_EMBEDTOGATE_CHAR;

    attr_md.read_perm  = p_mope_init->mops_MtoG_attr_md.read_perm;
    attr_md.write_perm = p_mope_init->mops_MtoG_attr_md.write_perm;
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 1;

    p_mops->MtoG = -1;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = sizeof(int);
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = sizeof(int);
    attr_char_value.p_value   = &p_mops->MtoG;

    return sd_ble_gatts_characteristic_add(p_mops->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_mops->MtoG_handles)
}

/* Add the insert success characteristic */
static uint32_t insert_success_char_add(ble_mops_t *p_mops, const ble_mops_init *p_mope_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;
    int                 initial_success;

    memset(&char_md, 0, sizeof(char_md));

    char_md.char_props.read     = 1;
    char_md.char_props.indicate = 1;
    char_md.p_char_user_desc    = NULL;
    char_md.p_char_pf           = NULL;
    char_md.p_user_desc_md      = NULL;
    char_md.p_cccd_md           = NULL;
    char_md.sccd_md             = NULL;

    ble_uuid.type = p_mops->uuid_type;
    ble_uuid.uuid = BLE_UUID_INSERTSUCCESS_CHAR;

    attr_md.read_perm  = p_mope_init->mops_insert_success_attr_md.read_perm;
    attr_md.write_perm = p_mope_init->mops_insert_success_attr_md.write_perm;
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 1;

    initial_success = 0;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = sizeof(int);
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = sizeof(int);
    attr_char_value.p_value   = &p_mops->insert_success;

    return sd_ble_gatts_characteristic_add(p_dops->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_dops->insert_success_handles);

}

/* Initialize the mOPE service */
uint32_t ble_mops_init(ble_mops_t *p_mops, const ble_mops_init_t *p_mope_init)
{
    uint32_t err_code;
    ble_uuid_t ble_uuid;
    ble_uuid128_t mope_uuid_base;

    memcopy(mope_uuid_base.uuid128, mope_uuid_base_val, 16*sizeof(char));

    // Add custom base UUID
    err_code = sd_ble_uuid_vs_add(&mope_uuid_base, &p_mops->uuid_type);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    // Initialize the service structure
    p_mops->evt_handler = p_mope_init->evt_handler;
    p_mops->conn_handle = BLE_CONN_HANDLE_INVALID;
    ble_uuid.type = p_mops->uuid_type;
    ble_uuid.uuid = BLE_UUID_MOPE_SERVICE;

    // Add service
    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY,
                                        &ble_uuid,
                                        &p_mops->service_handle);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    // Add cipher characteristic
    err_code = cipher_char_add(p_mops, p_mope_init);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    // Add GtoM characterisitic
    err_code = GtoM_char_add(p_mops, p_mope_init);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    // Add MtoG characteristic
    err_code = MtoG_char_add(p_mops, p_mope_init);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }
    // Add insert_success characteristic
    err_code = insert_success_char_add(p_mops, p_mope_init);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    // Allocate incoming data buffer
    uint32_t size = DEFAULT_BUFFER_SIZE*sizeof(uint16_t);
    uint8_t* buffer;
    err_code = nrf_sdk_mem_alloc(&buffer, &size);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    err_code = app_fifo_init(&p_mops->input_buffer, *buffer, (uint16_t) size);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    p_mops->blocking = 0;


    return NRF_SUCCESS;
}

/* Encrypt data, update the cipher characteristic, begin indicating
   that the cipher should now be read */
uint32_t ble_mops_send_data(ble_mops_t *p_mops, uint16_t data)
{
    // Send if connected and indicating
    if (p_mops->conn_handle != BLE_CONN_HANDLE_INVALID)
    {
        // Pad and encryt data
        uint8_t[16] plaintext;
        memset(plaintext, 0, sizeof(plaintext));  // pad with 0s
        memcpy(plaintext, &data, sizeof(uint16_t));  // add true plaintext
        bool crypt_success = nrf_ecb_crypt(&p_mops->cipher.data, plaintext); // AES ECB **NOT CPA SECURE!
        if (!crypt_success)
        {
            return ERROR_INTERNAL;
        }
        uint32_t err_code;
        uint16_t hvx_len;
        ble_gatts_hvx_params_t hvx_params;

        hvx_len = sizeof(cipher_t);

        memset(&hvx_params, 0, sizeof(hvx_params));

        hvx_params.handle = p_mops->cipher_handles.value_handle;
        hvx_params.type   = BLE_GATT_HVX_INDICATION;
        hvx_params.offset = 0;
        hvx_params.p_len  = &hvx_len;
        hvx_params.p_data = &p_mops->cipher;

        err_code = sd_ble_gatts_hvx(p_mops->conn_handle, &hvx_params);
        if ((err_code == NRF_SUCCESS) && (hvx_len != sizeof(cipher_t)))
        {
            err_code = NRF_ERROR_DATA_SIZE;
        } else if (err_code == NRF_SUCCESS)
        {
            p_mops->blocking = 1;
        }

    }
    else 
    {
        err_code = NRF_ERROR_INVALID_STATE;
    }

    return err_code;
}


/* Attempt to send data.  The only error code that does not indicate a failed 
   session is ERROR_BUSY.  NRF_ERROR_INVALID_STATE indicates the
   cipher characteristic has not set up an indicate session yet */
uint32_t ble_mops_insert(ble_mops_t *p_mops, uint16_t data)
{
    uint32_t err_code;
    // Enqueue if data is blocking 
    if (p_mops->blocking)
    {
        size_t len = sizeof(uint16_t);
        err_code = app_fifo_put_len(&p_mops->input_buffer, &data, &len);
        if (err_code != NRF_SUCCESS)
        {
            return err_code;
        }

    } else  // mOPE is not blocking!
    {
        err_code = ble_mops_send_data(p_mops, data);
    }
    return err_code;
}






