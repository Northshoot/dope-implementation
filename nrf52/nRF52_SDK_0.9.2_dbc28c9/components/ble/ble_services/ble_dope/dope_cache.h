#ifndef DOPE_CACHE_H
#define DOPE_CACHE_H

#include "dope_encoding.h"
#include "nordic_common.h"
#include <string.h>
#include "ble_srv_common.h"

#define MAX_ENTRIES 20
#define ENTRY_BYTES 8
/* A dOPE cache entry */
struct entry 
{
    uint16_t data;
    encoding_t encoding;
    uint16_t subtree_size;
    uint8_t  flags;
    int      lru;
};
typedef struct entry entry_t;


#include "dope_sync_queue.h"

/* Cache entry flags */
#define IS_LEAF 0x01
#define HAS_RIGHT_CHILD 0x02
#define HAS_LEFT_CHILD 0x04
#define SYNC 0x08
#define PRESENT 0x10

/* Bit setting vals */
#define LOW  0x00
#define HIGH 0x01

/* The cache structure */
struct cache 
{
    int              table_size;
    int              total_inserts;
    int              max_size;
    int              lru;
    bool             waiting_on_miss;
    bool             waiting_on_rebalance;
    encoding_t       start_encoding;
    encoding_t       rebalance_root;
    uint8_t          num_rebal_syncs;
    uint16_t         missing_plaintext;
    sync_queues_t    *sync_queues;
    float            sg_alpha;
    entry_t          table[MAX_ENTRIES];
};

/* Initialize the dope cache */
uint32_t cache_init(struct cache *cp, int max_size);

/* Insert data or continue with halted insert on provided data */
uint32_t cache_insert(uint16_t new_plaintext, encoding_t *start_enc);

/* Acknowledge that the provided entry has been synchronized between
   the sensor and the gateway */
bool cache_acknowledge_sync(encoding_t *enc);

/* Merge entry into cache from gateway tier.  Return true if there is
   room and the entry does not collide on an encoding */
bool cache_merge_entry(entry_t *entry);

#endif