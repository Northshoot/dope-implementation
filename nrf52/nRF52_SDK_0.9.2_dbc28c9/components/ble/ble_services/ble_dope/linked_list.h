#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include "nordic_common.h"
#include <string.h>
#include "ble_srv_common.h"
/* Credit to Stanford CS 144 course project 1, ctcp */

/* A node in the linked list structure */
struct ll_node
{
    void *data;
    struct ll_node *next;
    struct ll_node *prev;
};
typedef struct ll_node ll_node_t;

/* A linked list */
struct linked_list
{
    struct ll_node *head;
    struct ll_node *tail;
    unsigned int length;
};
typedef struct linked_list linked_list_t;  

/* Creates a new linked list and returns it.  Must be freed later with 
   ll_destroy. */
linked_list_t *ll_create();

/* Destroys a linked list.  This will free up its memory and the memory
   taken by its nodes, but not the memory of the data inside the nodes */ 
void ll_destroy(linked_list_t *list);

/* Adds an object to the back of the linked list */
void ll_push_back(linked_list_t *list, void *data);

/* Adds an object to the front of the linked list */
void ll_push_front(linked_list_t *list, void *data);

/* Searches and returns the node containing the specified object.
   If it cannot be found return NULL */
ll_node_t *ll_find(linked_list_t *list, void *data);

/* Removes a node from the linked list.  Frees up memory taken by node but
   not that taken by the object */
void *ll_remove(linked_list_t *list, ll_node_t *node);

/* Returns the length of the list */
uint8_t ll_length(linked_list_t *list);

#endif