#include "dope_encoding.h"
#include "dope_cache.h"
#include "nrf_error.h"

/* Return the minmum length of two encodings */
uint8_t min_len(encoding_t enc1, encoding_t enc2)
{
    if (enc1.enc_len < enc2.enc_len)
        return enc1.enc_len;
    else
        return enc2.enc_len;
}

/* Return true if the bit at bit_position in data is high.
   0 < bit_position < 31*/
bool bit_high(uint32_t data, uint8_t bit_position)
{
    uint32_t bit_flag = (0x00000001 << bit_position);
    data &= bit_flag;
    data = data >> bit_position;
    return data == 0x00000001;
}

/* Return true if encoding is root */
bool encoding_is_root(encoding_t *enc)
{
    return (enc->encoding == 0 && enc->enc_len == 0);
}

/* Append the appropriate bit to the encoding.  Fails if encoding is 32
   bits or longer. */
uint32_t encoding_append(uint8_t bit, encoding_t *enc)
{
    uint32_t err_code;
    if (enc->enc_len == 32)
    {
        err_code = NRF_ERROR_DATA_SIZE;
        return err_code;
    }
    err_code = NRF_SUCCESS;
    uint32_t mask = (0x00000001 << enc->enc_len);
    if (bit == HIGH)
    {
        enc->encoding |= mask;
    } else if (bit == LOW)
    {
        enc->encoding &= ~mask;
    } else
    {
        err_code = NRF_ERROR_INVALID_STATE;
        return err_code;
    }
    enc->enc_len++;
    return err_code;
}

/* Less than function for encodings.  Return 1 if enc1 < enc2,
   -1 if enc1 > enc2, 0 if enc1 = enc2 */
int encoding_less(encoding_t *enc1, encoding_t *enc2)
{ 
    uint8_t min = min_len(*enc1, *enc2);
    uint8_t bit_position = 0;
    while (bit_position < min)
    {
        if (!bit_high(enc1->encoding, bit_position) && 
            bit_high(enc2->encoding, bit_position))
        {
            return 1;
        } else if (bit_high(enc1->encoding, bit_position)
                   && !bit_high(enc2->encoding, bit_position))
        {
            return -1;
        }
        bit_position++;
    }
    if (enc1->enc_len < enc2->enc_len)
    {
        if (bit_high(enc2->encoding, bit_position))
        {
            return 1;
        }
        return -1;
    }
    else if (enc2->enc_len < enc1->enc_len)
    {
        if (bit_high(enc1->encoding, bit_position))
        {
            return -1;
        }
        return 1;
    }
    else
    {
        return 0;
    }
}

/* Return true if enc belongs to a subtree of root_enc */
bool encoding_subtree(encoding_t *root_enc, encoding_t *enc)
{
    if (enc->enc_len < root_enc->enc_len)
    {
        return false;
    }
    uint32_t root_enc_flag = 0x00000001;
    for (int i = 0; i < root_enc->enc_len; i++)
    {
        root_enc_flag = root_enc_flag << 1;
        root_enc_flag |= 0x00000001;
    }
    uint32_t enc_root_len = enc->encoding & root_enc_flag;
    uint32_t cmp = root_enc->encoding ^ enc_root_len;
    return cmp == 0; 
}



