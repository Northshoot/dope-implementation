#include "linked_list.h"
#include "dope_sync_queue.h"
#include "mem_manager.h"
#include "nrf_error.h"


sync_queues_t *sync_queues_init (unsigned int max_len)
{
    struct sync_queues *sq;
    linked_list_t *priority_queue;
    linked_list_t *base_queue;
    uint32_t err_code;
    uint32_t sq_size = sizeof(struct sync_queues);
    uint32_t ll_size = sizeof(linked_list_t);

    /* Allocate space for the sync queue struct and fields */
    err_code = nrf_sdk_mem_alloc((uint8_t **) &sq, &sq_size);
    if (err_code != NRF_SUCCESS)
    {
        return NULL;
    }
    err_code = nrf_sdk_mem_alloc((uint8_t **) &priority_queue, &ll_size);
    if (err_code != NRF_SUCCESS)
    {
        return NULL;
    }
    err_code = nrf_sdk_mem_alloc((uint8_t **) &base_queue, &ll_size);
    if (err_code != NRF_SUCCESS)
    {
        return NULL;
    }

    /* Set up the sync queues structure */
    sq->max_len = max_len;
    sq->base_len = 0;
    sq->priority_len = 0;
    sq->priority_queue = priority_queue;
    sq->base_queue = base_queue;
    return sq;
}

void prioritize(sync_queues_t *sq, entry_t *entry)
{
    /* Traverse the linked list looking for a matching entry 
       in the base queue */
    ll_node_t *ll_node = sq->base_queue->head;
    entry_t *ll_entry;
    while (ll_node != NULL)
    {
        ll_entry = (entry_t *) ll_node->data;
        if (ll_entry->encoding.encoding == entry->encoding.encoding &&
            ll_entry->encoding.enc_len == ll_entry->encoding.enc_len)
        {
            break;
        }
        ll_node = ll_node->next;
    }

    if (ll_node == NULL)
    {
        /* Precondition not satisfied */
        return;
    }

    ll_remove(sq->base_queue, ll_node);
    ll_push_back(sq->priority_queue, ll_entry);
    return;
}

bool put_base(sync_queues_t *sq, entry_t *entry)
{
    uint32_t err_code;
    entry_t *entry_copy;
    uint32_t entry_size = sizeof(entry_t);
    if (sq->base_len + sq->priority_len >= sq->max_len)
    {
        /* No room in the queue */
        return false;
    }
    err_code = nrf_sdk_mem_alloc((uint8_t **)&entry_copy, &entry_size);
    if (err_code != NRF_SUCCESS)
    {
        /* No memory left for the queue*/
        return false;
    }
    memcpy(entry_copy, entry, entry_size);
    sq->base_len++;
    ll_push_back(sq->base_queue, entry_copy);
    return true;
}

bool put_priority(sync_queues_t *sq, entry_t *entry)
{
    uint32_t err_code;
    entry_t *entry_copy;
    uint32_t entry_size = sizeof(entry_t);
    if (sq->base_len + sq->priority_len >= sq->max_len)
    {
        /* No room in the queue */
        return false;
    }
    err_code = nrf_sdk_mem_alloc((uint8_t **)&entry_copy, &entry_size);
    if (err_code != NRF_SUCCESS)
    {
        /* No memory left for the queue*/
        return false;
    }
    memcpy(entry_copy, entry, entry_size);
    sq->priority_len++;
    ll_push_back(sq->priority_queue, entry_copy);
    return true;
}

entry_t *pop_base(sync_queues_t *sq)
{
    if (sq->base_len == 0)
    {
        return NULL;
    }
    else
    {
        void *data = ll_remove(sq->base_queue, sq->base_queue->tail);
        return (entry_t *) data;
        sq->base_len--;
    }
}

entry_t *pop_priority(sync_queues_t *sq)
{
    if (sq->priority_len == 0)
    {
        return NULL;
    }
    else
    {
        void *data = ll_remove(sq->priority_queue, sq->priority_queue->tail);
        return (entry_t *) data;
        sq->priority_len--;
    }
}
