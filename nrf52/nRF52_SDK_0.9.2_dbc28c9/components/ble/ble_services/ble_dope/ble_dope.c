#include "ble_dope.h"
#include <string.h>
#include "nordic_common.h"
#include "ble_l2cap.h"
#include "ble_srv_common.h"
#include "app_util.h"
#include "mem_manager.h"
#include "nrf_ecb.h"
#include "sdk_common.h"

static unsigned char dope_uuid_base_val[16] = {0x35, 0xf9, 0x03, 0x8d, 0xa0, 0xbd, 0x4f, 0xff, 0x90, 0x31, 0x8a, 0x4e, 0x00, 0x00, 0x77, 0x58};
static uint8_t desc[4] = {0x53, 0x59, 0x4E, 0x43};

#define DOPE_MUTEX_LOCK()   SDK_MUTEX_LOCK(dope_mutex)
#define DOPE_MUTEX_UNLOCK() SDK_MUTEX_UNLOCK(dope_mutex)

SDK_MUTEX_DEFINE(dope_mutex)

/* Function for handling the Connect event. */
static void on_connect(ble_dops_t * p_dops, ble_evt_t * p_ble_evt)
{
    p_dops->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
}

/* Function for handling the Disconnect event. */
static void on_disconnect(ble_dops_t * p_dops, ble_evt_t * p_ble_evt)
{
    UNUSED_PARAMETER(p_ble_evt);
    p_dops->conn_handle = BLE_CONN_HANDLE_INVALID;
}

/* Function for handling write events to the dOPE sync meta characteristic's cccd */
static void on_sync_meta_cccd_write(ble_dops_t *p_dops, 
                               ble_gatts_evt_write_t *p_evt_write)
{
    if (p_evt_write->len == 2)
    {
        // CCCD written, update notification state
        if (p_dops->evt_handler != NULL)
        {
            ble_dops_evt_t evt;
            if (ble_srv_is_notification_enabled(p_evt_write->data))
            {
                evt.evt_type = BLE_DOPS_EVT_SYNC_INDICATION_ENABLED;
            }
            else
            {
                evt.evt_type = BLE_DOPS_EVT_SYNC_INDICATION_DISABLED;
            }

            p_dops->evt_handler(p_dops, &evt);
        }
    }
}


/* Function for handling write events to the dOPE sync cipher characteristic's cccd */
static void on_sync_cipher_cccd_write(ble_dops_t *p_dops, 
                                      ble_gatts_evt_write_t *p_evt_write)
{
    if (p_evt_write->len == 2)
    {
        // CCCD written, update notification state
        if (p_dops->evt_handler != NULL)
        {
            ble_dops_evt_t evt;
            if (ble_srv_is_notification_enabled(p_evt_write->data))
            {
                evt.evt_type = BLE_DOPS_EVT_SYNC_INDICATION_ENABLED;
            }
            else
            {
                evt.evt_type = BLE_DOPS_EVT_SYNC_INDICATION_DISABLED;
            }

            p_dops->evt_handler(p_dops, &evt);
        }
    }
}
/* Function for handling write events to the dOPE control message 
   characteristic cccd */
static void on_ctrl_msg_cccd_write(ble_dops_t *p_dops, 
                                   ble_gatts_evt_write_t *p_evt_write)
{
    if (p_evt_write->len == 2)
    {
        // CCCD written update indication state
        if (p_dops->evt_handler != NULL)
        {
            ble_dops_evt_t evt;
            if (ble_srv_is_indication_enabled(p_evt_write->data))
            {
                evt.evt_type = BLE_DOPS_EVT_CTRLMSG_INDICATION_ENABLED;
            }
            else
            {
                evt.evt_type = BLE_DOPS_EVT_CTRLMSG_INDICATION_ENABLED;
            }

            p_dops->evt_handler(p_dops, &evt);
        }
    }
}


/* Begin indicating the service's sync meta data field */
uint32_t ble_dops_send_sync_meta(ble_dops_t *p_dops)
{
    uint32_t err_code;

    // Send value if connected and indicating
    if (p_dops->conn_handle != BLE_CONN_HANDLE_INVALID)
    {
        uint16_t  hvx_len;
        ble_gatts_hvx_params_t hvx_params;

        hvx_len = ENTRY_BYTES;

        memset(&hvx_params, 0, sizeof(hvx_params));

        hvx_params.handle = p_dops->sync_meta_handles.value_handle;
        hvx_params.type   = BLE_GATT_HVX_INDICATION;
        hvx_params.offset = 0;
        hvx_params.p_len  = &hvx_len;
        hvx_params.p_data = p_dops->sync_meta_buf;

        err_code = sd_ble_gatts_hvx(p_dops->conn_handle, &hvx_params);
        if ((err_code == NRF_SUCCESS) && (hvx_len != ENTRY_BYTES))
        {
            err_code = NRF_ERROR_DATA_SIZE;
        }
    }
    else
    {
        err_code = NRF_ERROR_INVALID_STATE;
    }

    return err_code;
}

/* Begin indicating the servic'es sync ciphertext field */
uint32_t ble_dops_send_sync_cipher(ble_dops_t *p_dops)
{
    uint32_t err_code;

    // Send value if connected and indicating
    if (p_dops->conn_handle != BLE_CONN_HANDLE_INVALID)
    {
        uint16_t  hvx_len;
        ble_gatts_hvx_params_t hvx_params;

        hvx_len = CIPHER_BYTES;

        memset(&hvx_params, 0, sizeof(hvx_params));

        hvx_params.handle = p_dops->sync_cipher_handles.value_handle;
        hvx_params.type   = BLE_GATT_HVX_INDICATION;
        hvx_params.offset = 0;
        hvx_params.p_len  = &hvx_len;
        hvx_params.p_data = p_dops->sync_cipher_buf;

        err_code = sd_ble_gatts_hvx(p_dops->conn_handle, &hvx_params);
        if ((err_code == NRF_SUCCESS) && (hvx_len != CIPHER_BYTES))
        {
            err_code = NRF_ERROR_DATA_SIZE;
        }
    }
    else
    {
        err_code = NRF_ERROR_INVALID_STATE;
    }

    return err_code;


}

/* Begin indicating the services control message field */
uint32_t ble_dops_send_ctrl_msg(ble_dops_t *p_dops)
{
    uint32_t err_code;
    
    // Send value if connected and indicating
    if (p_dops->conn_handle != BLE_CONN_HANDLE_INVALID)
    {
        uint16_t hvx_len;
        ble_gatts_hvx_params_t hvx_params;
        hvx_len = CTRLMSG_BYTES;

        memset(&hvx_params, 0, sizeof(hvx_params));

        hvx_params.handle = p_dops->control_msg_handles.value_handle;
        hvx_params.type   = BLE_GATT_HVX_INDICATION;
        hvx_params.offset = 0;
        hvx_params.p_len  = &hvx_len;
        hvx_params.p_data = p_dops->ctrl_msg_buf;

        err_code = sd_ble_gatts_hvx(p_dops->conn_handle, &hvx_params);
        if ((err_code == NRF_SUCCESS) && (hvx_len != CTRLMSG_BYTES))
        {
            err_code = NRF_ERROR_DATA_SIZE;
        }
    }
    else
    {
        err_code = NRF_ERROR_INVALID_STATE;
    }

    return err_code;

}

/* Encode from structure to buffer to reduce transport space and ease
   in client unmarshalling of characteristic value.

   Precondition: buffer must be able to hold 8 bytes */
void ble_dops_encode_entry(entry_t *entry, uint8_t *buffer)
{
    /* Ignore entry data, begin with encoding */
    memcpy(buffer, &entry->encoding.encoding, sizeof(uint32_t));
    buffer += sizeof(uint32_t);
    memcpy(buffer, &entry->encoding.enc_len, sizeof(uint8_t));
    buffer += sizeof(uint8_t);
    
    /* Add subtree size */
    memcpy(buffer, &entry->subtree_size, sizeof(uint16_t));
    buffer += sizeof(uint16_t);

    /* Add the flag bits */
    memcpy(buffer, &entry->flags, sizeof(uint8_t));

    /* Ignore LRU */
    return;
}

/* Decode bytes into an entry structure.  

   Precondition: buffer is able to hold 8 bytes*/
void ble_dops_decode_entry(entry_t *entry, uint8_t *buffer)
{
    // Make sure buffer has the correct endianess
    memcpy(&entry->encoding.encoding, buffer, sizeof(uint32_t));
    buffer += sizeof(uint32_t);
    memcpy(&entry->encoding.enc_len, buffer, sizeof(uint8_t));
    buffer += sizeof(uint8_t);
    memcpy(&entry->subtree_size, buffer, sizeof(uint16_t));
    buffer += sizeof(uint16_t);
    memcpy(&entry->flags, buffer, sizeof(uint8_t));

    return;

}

/* Prepare p_dops for sending a the next enqueued sync to the gateway */
uint32_t ble_dops_marshall_sync(ble_dops_t *p_dops)
{
    entry_t *sync_entry = pop_priority(p_dops->cache.sync_queues);
    if (sync_entry == NULL)
    {
        sync_entry = pop_base(p_dops->cache.sync_queues);
        if (sync_entry == NULL)
         {
            return NRF_ERROR_INVALID_STATE;
         }
    }
    bool success = cache_acknowledge_sync(&sync_entry->encoding);
    if (!success)
    {
        return NRF_ERROR_INVALID_STATE;
    }

    /* Marshall metadata */
    ble_dops_encode_entry(sync_entry, p_dops->sync_meta_buf);

    /* Encrypt plaintext and marshall ciphertext */
    uint8_t plaintext[16];
    memset(plaintext, 0, sizeof(plaintext)); // pad with 0s
    memcpy(plaintext, &sync_entry->data, sizeof(uint16_t));
    nrf_sdk_mem_free((uint8_t *) sync_entry);

    success = nrf_ecb_crypt(p_dops->sync_cipher_buf, plaintext);
    if (!success)
    {
        return NRF_ERROR_INTERNAL;
    }
    return NRF_SUCCESS;
}

/* Insert into the dOPE cache and then handle related events over
   the network.  If the encoding tree traversal is successful 
   without misses then this call ends by transmitting a sync
   to the gateway.  If a miss occurs it ends by transmitting
   a control message to help the gateway handle the miss.  If a
   rebalance occurs a control message is sent asking the gateway
   for help handling the rebalance.

   PRECONDITION: All inserts have finished and the state of 
   the dope service structure has been set to inserting before
   calling. */
uint32_t ble_dops_cache_insert(ble_dops_t *p_dops, uint16_t data, 
                               encoding_t *p_enc)
{
    uint32_t err_code;
    ble_dops_state_t new_state;
    err_code = cache_insert(data, p_enc);
    /* If a duplicate is found we are finished
       without any syncing with higher tiers */
    if (err_code == NRF_ERROR_NULL)
    {
        DOPE_MUTEX_LOCK();
        p_dops->state = BLE_DOPS_READY;
        DOPE_MUTEX_UNLOCK();
        return NRF_SUCCESS;
    }
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    /* Insert successful, send sync */
    if (!p_dops->cache.waiting_on_miss && 
        !p_dops->cache.waiting_on_rebalance)
    {
        err_code = ble_dops_marshall_sync(p_dops);
        if (err_code != NRF_SUCCESS)
        {
            return err_code;
        }
        /* If Connected but CCCD not indicating yet, change state so 
           that the next insert will directly send the marshalled sync */
        DOPE_MUTEX_LOCK();
        p_dops->state = BLE_DOPS_SYNC_MARSHALLED;
        DOPE_MUTEX_UNLOCK();
        err_code = ble_dops_send_sync_meta(p_dops);
        if (err_code != NRF_SUCCESS)
        {
            return err_code;
        }
        new_state = BLE_DOPS_SYNC_META_WAIT;
    }
    /* Cache miss, send ctrl_msg */
    if (p_dops->cache.waiting_on_miss && 
        !p_dops->cache.waiting_on_rebalance)
    {
        /* Send control message */
        uint8_t msg = BLE_DOPS_CTRL_MISS;
        memcpy(&p_dops->ctrl_msg_buf, &msg, sizeof(uint8_t));
        memcpy(&p_dops->ctrl_msg_buf + sizeof(uint8_t), 
               &p_dops->cache.start_encoding.encoding, sizeof(uint32_t));
        memcpy(&p_dops->ctrl_msg_buf + sizeof(uint8_t) + sizeof(uint32_t),
               &p_dops->cache.start_encoding.enc_len, sizeof(uint8_t));
        err_code = ble_dops_send_ctrl_msg(p_dops);
        if (err_code != NRF_SUCCESS)
        {
            return err_code;
        }
        new_state = BLE_DOPS_CTRL_MSG_WAIT;
    }
    /* Cache rebalance, send ctrl_msg */
    if (p_dops->cache.waiting_on_rebalance && 
        !p_dops->cache.waiting_on_miss)
    {
        /* Send control message */
        uint8_t msg = BLE_DOPS_CTRL_REBAL;
        memcpy(&p_dops->ctrl_msg_buf, &msg, sizeof(uint8_t));
        memcpy(&p_dops->ctrl_msg_buf + sizeof(uint8_t), 
               &p_dops->cache.rebalance_root.encoding, sizeof(uint32_t));
        memcpy(&p_dops->ctrl_msg_buf + sizeof(uint8_t) + sizeof(uint32_t),
               &p_dops->cache.rebalance_root.enc_len, sizeof(uint8_t));
        err_code = ble_dops_send_ctrl_msg(p_dops);
        if (err_code != NRF_SUCCESS)
        {
            return err_code;
        }
        new_state = BLE_DOPS_CTRL_MSG_WAIT;
    }
    /* Invalid cache state */
    if (p_dops->cache.waiting_on_miss && 
        p_dops->cache.waiting_on_rebalance)
    {
        return NRF_ERROR_INVALID_STATE;
    }

    /* Update state */
    DOPE_MUTEX_LOCK();
    
    p_dops->state = new_state;
    
    DOPE_MUTEX_UNLOCK();

    return NRF_SUCCESS;
}

/* Continue taking data from buffer and inserting into the cache. 
   
   PRECONDITION: state should not already be BLE_DOPS_INSERTING */
uint32_t ble_dops_continue(ble_dops_t *p_dops)
{
    uint32_t err_code;
    uint16_t next_data;
    DOPE_MUTEX_LOCK();

    err_code = app_fifo_get_len(&p_dops->input_buffer, 
                                (uint8_t *)&next_data, 
                                sizeof(next_data));

    
    DOPE_MUTEX_UNLOCK();
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }
    DOPE_MUTEX_LOCK();
    p_dops->state = BLE_DOPS_INSERTING;
    DOPE_MUTEX_UNLOCK();
    err_code = ble_dops_cache_insert(p_dops, next_data, NULL);
    return err_code;
}


/* Function for handling write events to the dOPE miss response meta characteristic */
static void on_miss_response_meta(ble_dops_t *p_dops, ble_gatts_evt_write_t *p_evt_write)
{

    /* Record metadata and continue waiting for ciphertext */
    ble_dops_decode_entry(&p_dops->merge_entry, p_dops->miss_meta_buf);
    return;
    
}

/* Function for handling write events to the miss response cipher characteristic */
static void on_miss_response_cipher(ble_dops_t *p_dops, 
                                    ble_gatts_evt_write_t *p_evt_write)
{
    uint16_t merge_data;
    uint32_t err_code;
    /* Compute plaintext */
    uint8_t plaintext[16];
    bool success = nrf_ecb_crypt(plaintext, p_dops->miss_cipher_buf);
    if (!success)
    {
        return;// NRF_ERROR_INTERNAL;
    }
    memcpy(&merge_data, plaintext, sizeof(uint16_t));

    /* Read data into merge entry */
    p_dops->merge_entry.data = merge_data; 
    p_dops->merge_entry.lru = p_dops->cache.lru++;

    /* Merge entry into cache */
    success = cache_merge_entry(&p_dops->merge_entry);
    if (!success)
    {
        return;// NRF_ERROR_INVALID_STATE;
    }

    /* Change state to inserting */
    DOPE_MUTEX_LOCK();

    p_dops->state = BLE_DOPS_INSERTING;

    DOPE_MUTEX_UNLOCK();

    /* Finish insert */
    p_dops->cache.waiting_on_miss = false;
    err_code = ble_dops_cache_insert(p_dops, p_dops->cache.missing_plaintext, 
                                     &p_dops->merge_entry.encoding);
    if (err_code != NRF_SUCCESS)
    {
        return;// err_code;
    }

    return;// NRF_SUCCESS;

}


/* Function for handling write events to the dOPE rebal response characteristic */
static void on_rebal_response(ble_dops_t *p_dops, 
                              ble_gatts_evt_write_t *p_evt_write)
{
    uint32_t err_code;
    /* Check if there are more syncs to send, if so send them */
    if (p_dops->cache.num_rebal_syncs > 0)
    {
        err_code = ble_dops_marshall_sync(p_dops);
        if (err_code != NRF_SUCCESS)
        {
            return; //err_code;
        }
        err_code = ble_dops_send_sync_meta(p_dops); // Begin sync send
        if (err_code != NRF_SUCCESS)
        {
            return; //err_code;
        }        
    }
    else
    {
        /* When all syncs have been sent this write indicates cache insertion
           is once again safe. */
        err_code = ble_dops_continue(p_dops);
        (void)err_code;
    }
    return;// err_code;
}

/* Function for handling the Write event */
static void on_write(ble_dops_t * p_dops, ble_evt_t * p_ble_evt)
{
    ble_gatts_evt_write_t * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;
    if (p_evt_write->handle == p_dops->sync_meta_handles.cccd_handle){
        on_sync_meta_cccd_write(p_dops, p_evt_write);
    } else if (p_evt_write->handle == p_dops->sync_cipher_handles.cccd_handle) {
        on_sync_cipher_cccd_write(p_dops, p_evt_write);
    } else if (p_evt_write->handle == p_dops->control_msg_handles.cccd_handle) {
        on_ctrl_msg_cccd_write(p_dops, p_evt_write);
    } else if (p_evt_write->handle == p_dops->miss_meta_handles.value_handle) {
        on_miss_response_meta(p_dops, p_evt_write);
    } else if (p_evt_write->handle == p_dops->miss_cipher_handles.value_handle) {
        on_miss_response_cipher(p_dops, p_evt_write);
    } else if (p_evt_write->handle == p_dops->rebal_response_handles.value_handle) {
        on_rebal_response(p_dops, p_evt_write);
    }
}

/* Function for handling a sync indication ack */
static void on_hvc(ble_dops_t * p_dops, ble_evt_t * p_ble_evt)
{
    /* Only act if this comes from a sync characteristic */
    uint32_t err_code;
    DOPE_MUTEX_LOCK();

    /* Confirmed meta data? Now its time for cipher text. */
    if (p_dops->state == BLE_DOPS_SYNC_META_WAIT)
    {        
        err_code = ble_dops_send_sync_cipher(p_dops);
        if (err_code != NRF_SUCCESS)
        {
            DOPE_MUTEX_UNLOCK();
            return;
        }
        else
        {
            p_dops->state = BLE_DOPS_SYNC_CIPHER_WAIT;
            DOPE_MUTEX_UNLOCK();
            return;
        }
    }

    /* Confirmed ciphertext? Allow cache inserts to continue */
    if (p_dops->state == BLE_DOPS_SYNC_CIPHER_WAIT)
    {
        p_dops->state = BLE_DOPS_READY;
        DOPE_MUTEX_UNLOCK();
    }
    return;
}

/* Handle ble events */
void ble_dops_on_ble_evt(ble_dops_t * p_dops, ble_evt_t * p_ble_evt)
{
    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            on_connect(p_dops, p_ble_evt);
            break;
        case BLE_GAP_EVT_DISCONNECTED:
            on_disconnect(p_dops, p_ble_evt);
            break;
        case BLE_GATTS_EVT_WRITE:
            on_write(p_dops, p_ble_evt);
        case BLE_GATTS_EVT_HVC:
            on_hvc(p_dops, p_ble_evt);
            break;
    }
}
/* Add the sync meta data characteristic */
static uint32_t sync_meta_char_add(ble_dops_t * p_dops, 
                                   const ble_dops_init_t * p_dope_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_md_t cccd_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;

    // Indications are always supported
    memset(&cccd_md, 0, sizeof(cccd_md));
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    cccd_md.write_perm = p_dope_init->dops_sync_meta_attr_md.cccd_write_perm;
    cccd_md.vloc       = BLE_GATTS_VLOC_STACK;
    
    memset(&char_md, 0, sizeof(char_md));

    char_md.char_props.read   = 1;
    char_md.char_props.indicate = 1;
    char_md.p_char_user_desc  = desc;
    char_md.p_char_pf         = NULL;
    char_md.char_user_desc_max_size = 4;
    char_md.char_user_desc_size = 4;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = &cccd_md;
    char_md.p_sccd_md         = NULL;

    ble_uuid.type = p_dops->uuid_type;
    ble_uuid.uuid = BLE_UUID_SYNC_META_CHAR;

    attr_md.read_perm  = p_dope_init->dops_sync_meta_attr_md.read_perm;
    attr_md.write_perm = p_dope_init->dops_sync_meta_attr_md.write_perm;
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 1;


    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = ENTRY_BYTES;
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = ENTRY_BYTES;
    attr_char_value.p_value   = p_dops->sync_meta_buf;

    return sd_ble_gatts_characteristic_add(p_dops->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_dops->sync_meta_handles);

}

/* Add the sync ciphertext characteristic */
static uint32_t sync_cipher_char_add(ble_dops_t * p_dops, 
                                     const ble_dops_init_t * p_dope_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_md_t cccd_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;

    // Indications are always supported
    memset(&cccd_md, 0, sizeof(cccd_md));
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    cccd_md.write_perm = p_dope_init->dops_sync_cipher_attr_md.cccd_write_perm;
    cccd_md.vloc       = BLE_GATTS_VLOC_STACK;
    
    memset(&char_md, 0, sizeof(char_md));

    char_md.char_props.read   = 1;
    char_md.char_props.indicate = 1;
    char_md.p_char_user_desc  = desc;
    char_md.p_char_pf         = NULL;
    char_md.char_user_desc_max_size = 4;
    char_md.char_user_desc_size = 4;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = &cccd_md;
    char_md.p_sccd_md         = NULL;

    ble_uuid.type = p_dops->uuid_type;
    ble_uuid.uuid = BLE_UUID_SYNC_CIPHER_CHAR;

    attr_md.read_perm  = p_dope_init->dops_sync_cipher_attr_md.read_perm;
    attr_md.write_perm = p_dope_init->dops_sync_cipher_attr_md.write_perm;
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 1;


    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = CIPHER_BYTES;
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = CIPHER_BYTES;
    attr_char_value.p_value   = p_dops->sync_cipher_buf;

    return sd_ble_gatts_characteristic_add(p_dops->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_dops->sync_cipher_handles);

}

/* Add the control message characteristic */
static uint32_t control_message_char_add(ble_dops_t *p_dops, 
                                        const ble_dops_init_t * p_dope_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_md_t cccd_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;

    memset(&cccd_md, 0, sizeof(cccd_md));
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    cccd_md.write_perm = p_dope_init->dops_ctrl_msg_attr_md.cccd_write_perm;
    cccd_md.vloc       = BLE_GATTS_VLOC_STACK;

    memset(&char_md, 0, sizeof(char_md));

    char_md.char_props.read     = 1;
    char_md.char_props.indicate = 1;
    char_md.p_char_user_desc    = NULL;
    char_md.p_char_pf           = NULL;
    char_md.p_user_desc_md      = NULL;
    char_md.p_cccd_md           = &cccd_md;
    char_md.p_sccd_md           = NULL;

    ble_uuid.type = p_dops->uuid_type;
    ble_uuid.uuid = BLE_UUID_CTRLMSG_CHAR;

    attr_md.read_perm  = p_dope_init->dops_ctrl_msg_attr_md.read_perm;
    attr_md.write_perm = p_dope_init->dops_ctrl_msg_attr_md.write_perm;
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 1;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = CTRLMSG_BYTES;
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = CTRLMSG_BYTES;
    attr_char_value.p_value   = p_dops->ctrl_msg_buf;

    return sd_ble_gatts_characteristic_add(p_dops->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_dops->control_msg_handles);

}

/* Add the response message characteristic */
static uint32_t rebal_response_message_char_add (ble_dops_t *p_dops,
                                                 const ble_dops_init_t * p_dope_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;


    memset(&char_md, 0, sizeof(char_md));

    char_md.char_props.read     = 1;
    char_md.char_props.indicate = 1;
    char_md.p_char_user_desc    = NULL;
    char_md.p_char_pf           = NULL;
    char_md.p_user_desc_md      = NULL;
    char_md.p_cccd_md           = NULL;
    char_md.p_sccd_md           = NULL;

    ble_uuid.type = p_dops->uuid_type;
    ble_uuid.uuid = BLE_UUID_REBAL_RESPONSE_CHAR;

    attr_md.read_perm  = p_dope_init->dops_rebal_resp_attr_md.read_perm;
    attr_md.write_perm = p_dope_init->dops_rebal_resp_attr_md.write_perm;
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 1;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = RRESP_BYTES;
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = RRESP_BYTES;
    attr_char_value.p_value   = p_dops->rebal_resp_buf;

    return sd_ble_gatts_characteristic_add(p_dops->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_dops->rebal_response_handles);

}


/* Add the response message meta data characteristic */
static uint32_t miss_meta_char_add (ble_dops_t *p_dops,
                                    const ble_dops_init_t * p_dope_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;


    memset(&char_md, 0, sizeof(char_md));

    char_md.char_props.read     = 1;
    char_md.char_props.indicate = 1;
    char_md.p_char_user_desc    = NULL;
    char_md.p_char_pf           = NULL;
    char_md.p_user_desc_md      = NULL;
    char_md.p_cccd_md           = NULL;
    char_md.p_sccd_md           = NULL;

    ble_uuid.type = p_dops->uuid_type;
    ble_uuid.uuid = BLE_UUID_MISS_META_CHAR;

    attr_md.read_perm  = p_dope_init->dops_miss_meta_attr_md.read_perm;
    attr_md.write_perm = p_dope_init->dops_miss_meta_attr_md.write_perm;
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 1; 

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = ENTRY_BYTES;
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = ENTRY_BYTES;
    attr_char_value.p_value   = p_dops->miss_meta_buf;

    return sd_ble_gatts_characteristic_add(p_dops->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_dops->miss_meta_handles);
}

/* Add the response message cipher data characteristic */
static uint32_t miss_cipher_char_add (ble_dops_t *p_dops,
                                      const ble_dops_init_t * p_dope_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;


    memset(&char_md, 0, sizeof(char_md));

    char_md.char_props.read     = 1;
    char_md.char_props.indicate = 1;
    char_md.p_char_user_desc    = NULL;
    char_md.p_char_pf           = NULL;
    char_md.p_user_desc_md      = NULL;
    char_md.p_cccd_md           = NULL;
    char_md.p_sccd_md           = NULL;

    ble_uuid.type = p_dops->uuid_type;
    ble_uuid.uuid = BLE_UUID_MISS_CIPHER_CHAR;

    attr_md.read_perm  = p_dope_init->dops_miss_cipher_attr_md.read_perm;
    attr_md.write_perm = p_dope_init->dops_miss_cipher_attr_md.write_perm;
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 1; 

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = CIPHER_BYTES;
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = CIPHER_BYTES;
    attr_char_value.p_value   = p_dops->miss_cipher_buf;

    return sd_ble_gatts_characteristic_add(p_dops->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_dops->miss_cipher_handles);
}

/* Eventually we will need to install the data type (float vs int, and size
   and have corresponding comparison functions (maybe c already handles that)
   and have data pointed to by void* pointers */
/* Initialize the dOPE service */
uint32_t ble_dops_init(ble_dops_t * p_dops, const ble_dops_init_t *p_dope_init)
{
    uint32_t err_code;
    ble_uuid_t ble_uuid;
    ble_uuid128_t dope_uuid_base;

    memcpy(dope_uuid_base.uuid128, dope_uuid_base_val, 16*sizeof(char));

    // Add custom base UUID
    err_code = sd_ble_uuid_vs_add(&dope_uuid_base, &p_dops->uuid_type);
    if (err_code != NRF_SUCCESS) 
    {
        return err_code;
    }

    // Initialize the service structure
    p_dops->evt_handler = p_dope_init->evt_handler;
    p_dops->conn_handle = BLE_CONN_HANDLE_INVALID;
    ble_uuid.type = p_dops->uuid_type;
    ble_uuid.uuid = BLE_UUID_DOPE_SERVICE;

    // Add service
    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY,
                                        &ble_uuid,
                                        &p_dops->service_handle);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    // Add sync metadata characteristic
    err_code = sync_meta_char_add(p_dops, p_dope_init);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    // Add sync ciphertext characteristic
    err_code = sync_cipher_char_add(p_dops, p_dope_init);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    // Add control message characteristic
    err_code = control_message_char_add(p_dops, p_dope_init);
    if (err_code != NRF_SUCCESS)
    {
         return err_code;
    }

    // Add response characteristics
    err_code = rebal_response_message_char_add(p_dops, p_dope_init);
    if (err_code != NRF_SUCCESS)
    {
         return err_code;
    }

    err_code = miss_meta_char_add(p_dops, p_dope_init);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    err_code = miss_cipher_char_add(p_dops, p_dope_init);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    // Allocate incoming data buffer
    uint32_t size = p_dope_init->buf_size * sizeof(uint16_t);
    uint8_t* buffer;
    err_code = nrf_sdk_mem_alloc(&buffer, &size);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }
    err_code = app_fifo_init(&p_dops->input_buffer, buffer, size);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    // Initialize cache
    err_code = cache_init(&p_dops->cache, p_dope_init->entry_number);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    p_dops->state = BLE_DOPS_READY;


    return NRF_SUCCESS;
}



/* Feed data into the dOPE pipeline.  If an insert is ongoing place
   data into the insert buffer.  If the insert buffer is full return
   NRF_ERROR_NO_MEM and consider this data dropped.  If data is 
   inserted correctly into the data buffer or cache then return
   NRF_SUCCESS.  Other errors indicate a more serious failure of
   the cache system */
uint32_t ble_dops_insert(ble_dops_t *p_dops, uint16_t data)
{
    uint32_t err_code;
    bool pipeline_in_progress, retry;
    DOPE_MUTEX_LOCK();

    err_code = app_fifo_put_len(&p_dops->input_buffer, 
                                                (uint8_t *) &data, 
                                                sizeof(data));
    pipeline_in_progress = p_dops->state != BLE_DOPS_READY;
    retry = p_dops->state == BLE_DOPS_SYNC_MARSHALLED;
    DOPE_MUTEX_UNLOCK();
    /* Handle the case where a connection has been established but 
       the cccd's haven't yet been written */
    if (retry)
    {
        err_code = ble_dops_send_sync_meta (p_dops);
        if (err_code == NRF_SUCCESS)
        {
            DOPE_MUTEX_LOCK();
            p_dops->state = BLE_DOPS_SYNC_META_WAIT;
            DOPE_MUTEX_UNLOCK();
        }
        else
        {
            return NRF_ERROR_INVALID_STATE;
        }
    }
    /*if (err_code != NRF_SUCCESS)  Log drops here
    {
        return err_code;           //Dropped some data 
    }*/
    /* Wait on a gateway response before continuing with inserts */
    if (pipeline_in_progress) 
    {
        return NRF_SUCCESS;
    }
    else if (p_dops->conn_handle == BLE_CONN_HANDLE_INVALID)
    {
        /* Pipeline can't start without a valid connection */
        return NRF_ERROR_INVALID_STATE;
    }
    else
    {
        /* Begin a new pipeline */
        err_code = ble_dops_continue(p_dops);
        if (err_code == NRF_ERROR_NOT_FOUND)
        {
            // The entire buffer has been cleared
            return NRF_SUCCESS;
        }
        return err_code;
    }
}



