#ifndef DOPE_ENCODING_H
#define DOPE_ENCODING_H

#include "nordic_common.h"
#include <string.h>
#include "ble_srv_common.h"

/* A dOPE encoding.  The buttom enc_len bits fo encoding are active.
   The high bits are zeroed out.  enc_len = 0 implies a root encoding
   or an undefined encoding.  enc_len <= 32. */

/* A dOPE tree encoding */
typedef struct encoding
 {
    uint32_t encoding;
    uint8_t  enc_len;
} encoding_t;

/* Less than function for encodings.  Return 1 if enc1 < enc2,
   -1 if enc1 > enc2, 0 if enc1 = enc2 */
int encoding_less(encoding_t *enc1, encoding_t *enc2);

/* Return true if the encoding indicates a root entry */
bool encoding_is_root(encoding_t *enc);

/* Append the specified bit to the encoding.  Fails if encoding is
  already 32 bits long */
uint32_t encoding_append(uint8_t bit, encoding_t *enc);

/* Determine if enc is in the subtree rooted at root_enc */
bool encoding_subtree(encoding_t *root_enc, encoding_t *enc);

#endif