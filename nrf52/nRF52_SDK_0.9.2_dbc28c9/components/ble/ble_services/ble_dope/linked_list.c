#include "linked_list.h"
#include "mem_manager.h"

linked_list_t *ll_create() 
{
    linked_list_t *list;
    uint32_t err_code;
    uint32_t ll_size = sizeof(linked_list_t);
    err_code = nrf_sdk_mem_alloc((uint8_t **)&list, &ll_size);
    if (err_code != NRF_SUCCESS)
    {
        /* Not enough memory for list */
        return NULL;
    }
    list->head = NULL;
    list->tail = NULL;
    list->length = 0;
    return list;
}

void ll_destroy(linked_list_t *list)
{
    if (list == NULL)
        return;

    ll_node_t *curr = list->head;
    ll_node_t *next = NULL;
    while (curr != NULL) 
    {
        next = curr->next;
        nrf_sdk_mem_free((uint8_t *)curr);
        curr = next;
    }
    nrf_sdk_mem_free((uint8_t *) list);
}

ll_node_t *ll_create_node(void *data)
{
    ll_node_t *node;
    uint32_t err_code;
    uint32_t ll_node_size = sizeof(ll_node_t);
    err_code = nrf_sdk_mem_alloc((uint8_t **)&node, &ll_node_size);
    if (err_code != NRF_SUCCESS)
    {
        /* Not enough memory for node */
        return NULL;
    }
    node->next = NULL;
    node->prev = NULL;
    node->data = data;
    return node;
}

void ll_push_back(linked_list_t *list, void *data)
{
    if (list == NULL || data == NULL)
        return;

    ll_node_t *node = ll_create_node(data);
    /* List is empty */
    if (list->head == NULL)
    {
        list->head = node;
        list->tail = node;
    }

    /* List has one or more elements */
    else 
    {
        list->tail->next = node;
        node->prev = list->tail;
        list->tail = node;
    }

    list->length++;
    return;
}

void ll_push_front(linked_list_t *list, void *data)
{
    if (list == NULL || data == NULL)
        return;

    ll_node_t *node = ll_create_node(data);
    /* List is empty */
    if (list->head == NULL)
    {
        list->head = node;
        list->tail = node;
    }

    /* List has one or more elements */
    else 
    {
        node->next = list->head;
        list->head->prev = node;
        list->head = node;
    }

    list->length++;
    return;
}

ll_node_t *ll_find(linked_list_t *list, void *data)
{
    if (list == NULL || data == NULL)
        return NULL;

    ll_node_t *curr = list->head;
    while (curr != NULL)
    {
        if (curr->data == data)
        {
            return curr;
        }
        curr = curr->next;
    }
    return NULL;
}

void *ll_remove(linked_list_t *list, ll_node_t *node)
{
    if (list == NULL || node == NULL)
        return NULL;
    void *data = node->data;

    /* Update linked list pointers */
    if (node == list->head)
        list->head = node->next;
    else
        node->prev->next = node->next;

    if (node == list->tail)
        list->tail = node->prev;
    else
        node->next->prev = node->prev;

    /* Free memory */
    nrf_sdk_mem_free((uint8_t *) node);
    list->length--;

    return data;
}

uint8_t ll_length(linked_list_t *list)
{
    return list->length;
}

