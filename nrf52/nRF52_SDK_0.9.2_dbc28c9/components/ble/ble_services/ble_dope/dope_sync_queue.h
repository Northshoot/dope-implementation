#ifndef DOPE_SYNC_QUEUE_H
#define DOPE_SYNC_QUEUE_H

#include "linked_list.h"

/* The dope sync queue structure used to keep track of entries waiting to be
   synced with the gateway */
struct sync_queues 
{
    unsigned int max_len;
    unsigned int base_len;
    unsigned int priority_len;
    linked_list_t *priority_queue;
    linked_list_t *base_queue;
};
typedef struct sync_queues sync_queues_t;

#include "dope_cache.h"

/* Initialize the sync_queues */
sync_queues_t *sync_queues_init (unsigned int max_len);

/* Move matching entry from the base queue into the priority queue 

   Precondition: entry must already be in the base queue */
void prioritize(sync_queues_t *queues, entry_t *entry);

/* Put entry into slot on the base queue.  Return false if not enough room 
   on queue or if memory allocation fails */
bool put_base(sync_queues_t *queues, entry_t *entry);

/* Put data into slot on the priority queue. Return false if not enough room 
   on queue or if memory allocation fails */
bool put_priority(sync_queues_t *queues, entry_t *entry);

/* Pop the back entry of the priority queue and return the last entry.  If 
   empty return NULL */
entry_t *pop_priority(sync_queues_t *queues);

/* Pop the back entry of the base queue and return the last entry.  If empty 
   return NULL */
entry_t *pop_base(sync_queues_t *queues);

#endif
