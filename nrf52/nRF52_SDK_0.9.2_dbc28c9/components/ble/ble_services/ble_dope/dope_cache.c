#include "dope_cache.h"
#include "mem_manager.h"

struct cache* cache; //The global cache model

/* Initialize the cache structure.  Set metadata to indicate an empty 
   cache allocate an array of entries of the specified size as well as
   a sync_queues structure of that size */
uint32_t cache_init(struct cache *pcache, int max_size)
{
    cache = pcache;
    cache->max_size = max_size;
    cache->table_size = 0;
    cache->lru = 0;
    cache->total_inserts = 0;
    cache->waiting_on_miss = false;
    cache->waiting_on_rebalance = false;
    cache->start_encoding.encoding = 0;
    cache->start_encoding.enc_len = 0;
    cache->rebalance_root.encoding = 0;
    cache->rebalance_root.enc_len = 0;
    cache->num_rebal_syncs = 0;
    cache->missing_plaintext = 0;
    cache->sg_alpha = 0.5;
    
    cache->sync_queues = sync_queues_init(max_size);
    if (cache->sync_queues == NULL)
    {
        return NRF_ERROR_NO_MEM;
    }
    /* Clear out cache memory */
    int i;
    for (i = 0; i < cache->max_size; i++)
    {
        memset (cache->table + i, 0, sizeof(entry_t));
    }

    return NRF_SUCCESS;

}

/* Find a free place in the cache and fill in a newly inserted entry. 

   Precondition: there must be a free slot in the table array */
entry_t *add_entry(uint16_t data, encoding_t *encoding)
{
    int i;
    for (i = 0; i < cache->max_size; i++)
    {
        if ((cache->table[i].flags & PRESENT) == 0)
        {
            break;
        }
    }
    /* Update new entry */
    cache->table[i].lru = cache->lru;
    cache->table[i].data = data;
    memcpy(&cache->table[i].encoding, encoding, sizeof (encoding_t));
    cache->table[i].flags = PRESENT | IS_LEAF;
    cache->table[i].subtree_size = 1;

    /* Update global state */
    cache->lru++;
    cache->total_inserts++;
    cache->table_size++;

    return &cache->table[i];
}

/* Copy data from provided entry structure into an entry on the outgoing
   sync queue. This fails if there is no more room in the sync queue. */
uint32_t add_sync(entry_t *entry_to_sync)
{
    bool success = put_base (cache->sync_queues, entry_to_sync);
    if (success)
    {
        return NRF_SUCCESS;
    }
    else
    {
        return NRF_ERROR_DATA_SIZE;
    }
}

/* Return a pointer to the entry in the cache with the provided encoding.
   If the cache does not contain this entry then return NULL. */
entry_t *entry_with_encoding(encoding_t *encoding)
{
    for (int i = 0; i < cache->max_size; i++)
    {
        /* Check if this is a valid cache table entry */
        if ((cache->table[i].flags & PRESENT) != 0)
        {
            if (encoding_less(&cache->table[i].encoding, encoding) == 0)
            {
                return &cache->table[i];
            }
        }
    }
    return NULL;
}

/* Return the left child entry of the entry with the provided encoding,
   or NULL if this entry has no left child in its cache */
entry_t *left_child(encoding_t *enc)
{
    encoding_t left_enc;
    memcpy(&left_enc, enc, sizeof(encoding_t));
    encoding_append(LOW, &left_enc);
    return entry_with_encoding(&left_enc);
}

/* Return the right child entry of the entry with the provided encoding,
   or NULL if this entry has no right child in its cache */
entry_t *right_child(encoding_t *enc)
{
    encoding_t right_enc;
    memcpy(&right_enc, enc, sizeof(encoding_t));
    encoding_append(HIGH, &right_enc);
    return entry_with_encoding(&right_enc);
}

/* Update all parents of the entry with this encoding to register the 
   addition of this entry into their subtree */
void update_parent_sizes(encoding_t *enc)
{
    uint8_t enc_len = enc->enc_len;
    enc->enc_len = 0;
    while (enc->enc_len < enc_len)
    {
        entry_t *next_entry = entry_with_encoding(enc);
        if (next_entry != NULL)
        {
            next_entry->subtree_size += 1;
        }
        enc->enc_len++;
    }
}

/* Evict a single element from the cache table.  If element is unsynced
   then push it to the front of the sync queue and register that an 
   eviction is occurring */
void evict() 
{
    int min_lru = cache->table[0].lru;
    int index = 0;
    for (int i = 0; i < cache->max_size; i++)
    {
        if ((cache->table[i].flags & PRESENT) != 0)
        {
            if (cache->table[i].lru < min_lru)
            {
                min_lru = cache->table[i].lru;
                index = i;
            }
        }
    }

    /* If unsynced move to higher priority sync queue */
    if ((cache->table[index].flags & SYNC) == 0)
    {
        prioritize(cache->sync_queues, &cache->table[index]);
    }

    /* Evict entry from table */
    cache->table[index].flags &= ~PRESENT;
    cache->table_size--;
}

/* Prepare the cache for a rebalance, reprioritizing necessary unsynced
   entries, indicating the number of syncs to grab, and setting the
   rebalance root field.  Returns any errors generated along the way */
uint32_t rebalance_start(entry_t *new_entry)
{
    memcpy(&cache->rebalance_root, &new_entry->encoding, sizeof(encoding_t));

    /* Remove rebalancing subtree from the cache */
    for (int i =0; i < cache->max_size; i++)
    {
        if ((cache->table[i].flags & PRESENT) != 0)
        {
            if (encoding_subtree(&cache->rebalance_root, 
                &cache->table[i].encoding))
            {
                /* This entry is in the evicting subtree */
                cache->table[i].flags &= ~PRESENT;
                cache->table_size--;
                /* Ensure next tier has knowledge of this data */
                if ((cache->table[i].flags & SYNC) == 0)
                {
                    prioritize(cache->sync_queues, &cache->table[i]);
                }
            }
        }
    }
    return NRF_SUCCESS;
}

/* Determine if this entry is a scapegoat node */
bool is_scapegoat_node(entry_t *entry) 
{
    entry_t *left_child_entry = left_child(&entry->encoding);
    entry_t *right_child_entry = right_child(&entry->encoding);

    if (left_child_entry != NULL)
    {
        if (left_child_entry->subtree_size > (entry->subtree_size * 
            cache->sg_alpha))
        {
            return true;
        }
    }
    if (right_child_entry != NULL)
    {
        if (right_child_entry->subtree_size > (entry->subtree_size *
            cache->sg_alpha))
        {
            return true;
        }
    }
    return false;
}

/* Run through the recentmost insertion path and determine if a rebalance
   is necessary.  If so push non-synced evictees to the front of the sync
   queue and register a rebalance. */
uint32_t rebalance_check(entry_t *new_entry)
{
    uint32_t err_code;
    encoding_t enc;
    memcpy(&enc, &new_entry->encoding, sizeof(encoding_t));

    enc.enc_len -= 1;
    while (enc.enc_len >= 0)
    {
        entry_t *next_entry = entry_with_encoding(&enc);
        if (next_entry == NULL)
        {
            err_code = NRF_ERROR_DATA_SIZE;
            return err_code;
        }
        if (is_scapegoat_node(next_entry))
        {
            cache->waiting_on_rebalance = true;
            err_code = rebalance_start(next_entry);
            return err_code;
        }
        if (enc.enc_len == 0)
        {
            break;
        }
        enc.enc_len -=1;
    }

    return NRF_SUCCESS;

}

/* Handle the case where the insertion of new_plaintext can't finish traversal
   because encoding enc with length enc_len is not in the sensor cache */
uint32_t handle_miss(uint16_t new_plaintext, encoding_t *enc)
{
    uint32_t err_code;
    if (cache->table_size == cache->max_size)
    {
        evict();
    }  
    cache->waiting_on_miss = true;
    cache->start_encoding.encoding = enc->encoding;
    cache->start_encoding.enc_len = enc->enc_len;
    cache->missing_plaintext = new_plaintext;

    /* Right now always return success */
    err_code = NRF_SUCCESS;
    return err_code;
}

/* Determine if entry is a leaf node in the encoding tree */
bool is_leaf(entry_t *entry)
{
    if ((entry->flags & IS_LEAF) != 0)
    {
        return true;
    } else
    {
        return false;
    }
}


/* Used for both fresh inserts and picking up on inserts after cache misses.
   Either terminates with an updated cache table with the new entry inserted,
   a new outgoing message requesting the next element in the traversal, or
   no change in the case a duplicate is found.  If called without enough space
   for a new sync can return NRF_ERROR_DATA_SIZE.  If a duplicate is found
   returns NRF_ERROR_NULL

   Precodition: only called after verifying there is room in cache table */
uint32_t cache_insert(uint16_t new_plaintext, encoding_t *start_enc)
{
    uint32_t err_code = NRF_SUCCESS;
    int current_plaintext;

    /* Handle an empty cache */
    if (cache->table_size == 0)
    {
        encoding_t root_enc;
        root_enc.enc_len = 0;
        root_enc.encoding = 0;
        if (cache->total_inserts == 0)
        {
            /* Inserting original root */
            entry_t *entry = add_entry(new_plaintext, &root_enc);

            err_code = add_sync(entry);
            return err_code;
        } else 
        {
            /* Recover from root rebalance */
            err_code = handle_miss(new_plaintext, &root_enc);
            return err_code;
        }
    }

    /* Insert into non-empty cache */
    encoding_t new_entry_enc;
    entry_t *current_entry;
    if (start_enc == NULL)
    {
        new_entry_enc.encoding = 0;
        new_entry_enc.enc_len = 0;
    } else
    {
        new_entry_enc.encoding = start_enc->encoding;
        new_entry_enc.enc_len = start_enc->enc_len;
    }

    /* Find the current entry */
    current_entry = entry_with_encoding(&new_entry_enc);
    current_plaintext = current_entry->data;

    while (!is_leaf(current_entry)) 
    {
        current_entry->lru = cache->lru;
        cache->lru++;
        current_plaintext = current_entry->data;
        if (current_plaintext == new_plaintext)
        {
            /* Found duplicate in the cache */
            cache->waiting_on_miss = false;
            cache->waiting_on_rebalance = false;
            return NRF_ERROR_NULL;
        }
        else if (current_plaintext > new_plaintext && 
                (current_entry->flags & HAS_LEFT_CHILD) == 0)
        {
            /* Insert new value as left child */
            current_entry->flags |= HAS_LEFT_CHILD;
            break;
        }
        else if (current_plaintext < new_plaintext &&
                 (current_entry->flags & HAS_RIGHT_CHILD) == 0)
        {
            /* Insert new value as right child */
            current_entry->flags |= HAS_RIGHT_CHILD;
            break;
        }
        else
        {
            /* Continue traversal */
            uint8_t bit;
            if (new_plaintext > current_plaintext)
            {
                bit = HIGH;
            } else
            {
                bit = LOW;
            }

            encoding_append(bit, &new_entry_enc);
            current_entry = entry_with_encoding(&new_entry_enc);
            if (current_entry == NULL)
            {
                err_code = handle_miss(new_plaintext, &new_entry_enc);
                return err_code;
            }
        }      
    }

    /* Traversed up to the parent entry of the new entry */
    cache->waiting_on_miss = false;
    cache->waiting_on_rebalance = false;
    current_entry->lru = cache->lru;
    cache->lru++;
    if (current_plaintext == new_plaintext)
    {
        /* Duplicate in the cache */
        return NRF_ERROR_NULL;
    }
    else if (new_plaintext < current_plaintext)
    {
        current_entry->flags |= HAS_LEFT_CHILD;
        encoding_append(LOW, &new_entry_enc);
    }
    else /* (new_plaintext > current_plaintext) */
    {
        current_entry->flags |= HAS_RIGHT_CHILD;
        encoding_append(HIGH, &new_entry_enc);
    }
    current_entry->flags &= ~IS_LEAF;
    update_parent_sizes(&new_entry_enc);
    if (cache->max_size == cache->table_size)
    {
        evict();
    }
    entry_t *new_entry = add_entry(new_plaintext, &new_entry_enc);
    if ((new_entry->flags & SYNC) == 0)
    {
        err_code = add_sync(new_entry);
        if (err_code != NRF_SUCCESS)
        {
            return err_code;
        }
    }
    err_code = rebalance_check(new_entry);
    return err_code;
}

/* Flip the SYNC flag of the provided entry in the cache table */
bool cache_acknowledge_sync(encoding_t *enc)
{
    for (int i = 0; i < cache->max_size; i++)
    {
        if ((cache->table[i].flags & PRESENT) != 0)
        {
            if (enc->encoding == cache->table[i].encoding.encoding &&
                enc->enc_len == cache->table[i].encoding.enc_len)
            {
                cache->table[i].flags |= SYNC;
                return true;
            }
        }
    }
    return false;

}

/* Merge entry into the cache.  Return true if successful, false if 
   there is no more room or the entry already exists in the cache */
bool cache_merge_entry(entry_t *entry)
{ 
    if (cache->table_size == cache->max_size)
    {
        return false;
    }
    int empty_index = 0;
    for (int i = 0; i < cache->max_size; i++)
    {
        if ((cache->table[i].flags & PRESENT) != 0)
        {
            if (entry->encoding.encoding == cache->table[i].encoding.encoding &&
                entry->encoding.enc_len == cache->table[i].encoding.enc_len)
            {
                /* Fail merge on duplicate */
                return false;
            }
        }
        else
        {
            empty_index = i;
        }
    }

    /* Merge entry into empty_index */
    memcpy(&cache->table[empty_index], entry, sizeof(entry_t));

    cache->table[empty_index].flags |= PRESENT;
    cache->table[empty_index].lru = cache->lru++;
    cache->table_size++;  //Only update as already inserted and flushed
    return true;
}


