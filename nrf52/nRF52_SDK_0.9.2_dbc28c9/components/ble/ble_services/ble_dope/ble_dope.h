/** @file
 *
 * @{
 * @brief Distributed Order Preserving Encoding Service module.
 *
 * @details This module implements the dOPE Service with the Sensor Message and 
 *          Gateway Message characteristics.  During initialization it adds these
 *          characteristics to the BLE stack database.  
 *
 *          Notification of the Sensor Message characteristic is performed
 *          when the application calls ble_dOPE_sensor_message_send().
 *
 *          The dOPE Service also provides a set of functions for manipulating the
 *          various fields in the dOPE Message characteristics.
 *
 *          If an event handler is supplied by the application, the dOPE Service will
 *          generate dOPE Service events to the application.
 *
 * @note The application must propagate BLE stack events to the dOPE Service module by calling
 *       ble_dos_on_ble_evt() from the @ref softdevice_handler callback.
 */

#ifndef BLE_DOPS_H__
#define BLE_DOPS_H__
#define BLE_UUID_DOPE_SERVICE 0x0001
#define BLE_UUID_SYNC_META_CHAR 0x0002
#define BLE_UUID_SYNC_CIPHER_CHAR 0x0003
#define BLE_UUID_CTRLMSG_CHAR 0x0004
#define BLE_UUID_MISS_META_CHAR 0x0005
#define BLE_UUID_MISS_CIPHER_CHAR 0x0006
#define BLE_UUID_REBAL_RESPONSE_CHAR 0x0007

#include <stdint.h>
#include <stdbool.h>
#include "ble.h"
#include "ble_srv_common.h"
#include "dope_cache.h"
#include "nrf_soc.h"
#include "dope_sync_queue.h"
#include "app_fifo.h"



//Forward declare dOPE Service type
typedef struct ble_dops_s ble_dops_t;


/* dOPE Service event type */
typedef enum 
{
    BLE_DOPS_EVT_SYNC_INDICATION_ENABLED,
    BLE_DOPS_EVT_SYNC_INDICATION_DISABLED,
    BLE_DOPS_EVT_CTRLMSG_INDICATION_ENABLED,
    BLE_DOPS_EVT_CTRLMSG_INDICATION_DISABLED
} ble_dops_evt_type_t;


/* dOPE Service event */
typedef struct 
{
    ble_dops_evt_type_t evt_type;             /* Type of event */
} ble_dops_evt_t;

/**@brief dOPE Service event handler type. */
typedef void (*ble_dops_evt_handler_t) (ble_dops_t * p_dops, ble_dops_evt_t *p_evt);

struct ble_dops_init
{
    ble_dops_evt_handler_t       evt_handler;
    ble_srv_cccd_security_mode_t dops_sync_meta_attr_md;
    ble_srv_cccd_security_mode_t dops_sync_cipher_attr_md;
    ble_srv_cccd_security_mode_t dops_ctrl_msg_attr_md;
    ble_srv_cccd_security_mode_t dops_miss_meta_attr_md;
    ble_srv_cccd_security_mode_t dops_miss_cipher_attr_md;
    ble_srv_cccd_security_mode_t dops_rebal_resp_attr_md;
    size_t                       buf_size;
    size_t                       entry_number;
};
typedef struct ble_dops_init ble_dops_init_t;

typedef enum 
{
    BLE_DOPS_INSERTING,        //Data being fed to dOPE pipeline
    BLE_DOPS_SYNC_MARSHALLED,  //A sync is ready to send but last send was unsuccessful, try again
    BLE_DOPS_SYNC_META_WAIT,   //Sync meta data is ready to be read
    BLE_DOPS_SYNC_CIPHER_WAIT, //Sync ciphertext ready to be read
    BLE_DOPS_CTRL_MSG_WAIT,    //Blocking on control message
    BLE_DOPS_RESPONSE_WAIT,    //Blocking on a response from gateway
    BLE_DOPS_READY             //Ready to try a new message
} ble_dops_state_t;

/* An AES encrypted plaintext */
struct cipher
{
    uint8_t data[128];
};
typedef struct cipher cipher_t;
#define CIPHER_BYTES 16 // Cipher text total bytes, sync_cipher and rebal_miss
#define ENTRY_BYTES 8   // Compressed entry total bytes
#define CTRLMSG_BYTES 6 // Control message total bytes
#define RRESP_BYTES 1   // Rebalance response total bytes
/* A ciphertext, entry meta data pair.  Data type of sync and response 
   messages */
struct sync
{
    entry_t entry_meta_data;
    cipher_t entry_cipher_text;
};
typedef struct sync sync_t;


typedef enum 
{
    BLE_DOPS_CTRL_MISS,
    BLE_DOPS_CTRL_REBAL
} dope_control_t;
    
/* A message from sensor to gateway */
struct dope_control_msg
{
    dope_control_t ctrl;          // Either REBAL or MISS
    encoding_t enc;               // Missing encoding or rebal root encoding 
};
typedef struct dope_control_msg dope_control_msg_t;

/* The gateway response to a cache miss control message */
struct dope_missing_response_msg
{
    sync_t response_entry;
};
typedef struct dope_missing_response_msg dope_miss_response_msg_t;

/* The gateway response to a rebalance control message */
struct dope_rebal_response_msg
{
    bool gateway_ready;
};
typedef struct dope_rebal_response_msg dope_rebal_response_msg_t;

/* The dOPE ble service structure */
struct ble_dops_s {
    /* BLE service handles */
    ble_dops_evt_handler_t       evt_handler;                                          /**< Event handler to be called for handling events in the dOPE Service. */
    uint16_t                     service_handle;                                       /**< Handle of dOPE Service (as provided by the BLE stack). */
    ble_gatts_char_handles_t     sync_meta_handles;                                         /**< Handles related to the Synce characteristic. */
    ble_gatts_char_handles_t     sync_cipher_handles;
    ble_gatts_char_handles_t     control_msg_handles;                                  /**< Handles related to the control message characteristic. */
    ble_gatts_char_handles_t     miss_meta_handles;                                /**< Handles related to the meta data of a response to a miss control message >**/
    ble_gatts_char_handles_t     miss_cipher_handles;                             /**<Handles relate to the cipher text of a response to a miss controll message >**/
    ble_gatts_char_handles_t     rebal_response_handles;                               /**< Handles related to the response to a rebal control message >**/
    uint16_t                     conn_handle;                                          /**< Handle of the current connection (as provided by the BLE stack, is BLE_CONN_HANDLE_INVALID if not in a connection). */
    uint8_t                      uuid_type;                                           /**< Custom UUID type for DOPE >**/
    /* Data storing characteristic values */
    entry_t                      merge_entry;
    uint8_t                      sync_meta_buf[8];
    uint8_t                      sync_cipher_buf[16];
    uint8_t                      ctrl_msg_buf[6];
    uint8_t                      miss_meta_buf[8];
    uint8_t                      miss_cipher_buf[16];
    uint8_t                      rebal_resp_buf[1];

    /* BLE service state data */
    ble_dops_state_t             state;
    app_fifo_t                   input_buffer;
    nrf_mutex_t                  input_buffer_mutex;
    struct cache                 cache;
};


/* Function for handling the Application's BLE Stack events.  Handles
   all events from the BLE stack of interest to the dOPE Service */
uint32_t ble_dops_init(ble_dops_t *p_dops, const ble_dops_init_t *p_dops_init);

/* Function for initializing the dOPE Service */
void ble_dops_on_ble_evt(ble_dops_t *p_dops, ble_evt_t * p_ble_evt);

/* Function for sending heart rate measurement if notification has been enabled */
uint32_t ble_dops_insert(ble_dops_t *p_dops, uint16_t data);

#endif // BLE_DOPS_H__
 
