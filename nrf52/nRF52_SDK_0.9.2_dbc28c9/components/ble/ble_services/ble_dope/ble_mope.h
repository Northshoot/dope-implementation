#include ble_gatt.h
#include ble_gap.h

/** @file
 *
 * @{
 * @brief Mutable Order Preserving Encoding Service module.
 *
 * @details This module implements the mOPE Service with the sync, 
 *          control message and response characteristics. 
 *          During initialization it adds these
 *          characteristics to the BLE stack database.  
 *
 *          The mOPE Service also provides a set of functions for manipulating the
 *          various fields in the mOPE Message characteristics.
 *
 *          If an event handler is supplied by the application, the dOPE Service will
 *          generate dOPE Service events to the application.
 *
 * @note The application must propagate BLE stack events to the dOPE Service module by calling
 *       ble_dops_on_ble_evt() from the @ref softdevice_handler callback.
 */

 #ifndef BLE_DOPS_H__
#define BLE_MOPS_H__
#define BLE_UUID_MOPE_SERVICE 0x0001
#define BLE_UUID_CIPHER_CHAR 0x0002
#define BLE_UUID_GATETOEMBED_CHAR 0x0003
#define BLE_UUID_EMBEDTOGATE_CHAR 0x0004 
#define BLE_UUID_INSERTSUCCESS_CHAR 0x0005                       

#define DEFAULT_BUFFER_SIZE 100
#include <stdint.h>
#include <stdbool.h>
#include "ble.h"
#include "ble_srv_common.h"
#include "app_fifo.h"


/* Forward declare mOPE Service type */
typedef struct ble_mops_s ble_mops_t;

/* mOPE Service event type */
typedef enum 
{
    BLE_MOPS_EVT_CIPHER_INDICATION_ENABLED,
    BLE_MOPS_EVT_CIPHER_INDICATION_DISABLED,
    BLE_MOPS_EVT_RESPONSE_INDICATION_ENABLED,
    BLE_MOPS_EVT_RESPONSE_INDICATION_ENABLED,
    BLE_MOPS_EVT_COMPARE_REQUEST,
    BLE_MOPS_EVT_INSERT_COMPLETE_WRITE
} ble_mops_evt_type_t;

/* mOPE Service event */
typedef struct 
{
    ble_mops_evt_type_t evt_type;
} ble_mops_evt_t;

/* Cipher type*/
struct cipher 
{
    uint8_t[128] data;
}; 

typedef struct cipher cipher_t;

/* Gateway to embedded message type. Comparison request between c1 and c2 */
typedef struct GtoM 
{
    cipher_t c1;
    cipher_t c2;
} GtoM_t;


/* mOPE Service event handler type. */
typedef void (*ble_mops_evt_handler_t) (ble_mops_t * p_mops);

typedef struct 
{
    ble_mops_evt_handler_t       evt_handler;
    ble_srv_cccd_security_mode_t mops_cipher_attr_md;
    ble_srv_cccd_security_mode_t mops_MtoG_attr_md;
    uint8_t*                     initial_cipher;
} ble_mops_init_t;

/* The mOPE ble service structure */
struct ble_mops_s 
{
    ble_mops_evt_handler_t      evt_handler;
    uint16_t                    service_handle;
    ble_gatts_char_handles_t    cipher_handles;
    ble_gatts_char_handles_t    GtoM_handles;           // Gateway to embedded device messages
    ble_gatts_char_handles_t    MtoG_handles;           // Embedded device to gateway messages
    ble_gatts_char_handles_t    insert_success_handles;
    uint16_t                    conn_handle;
    uint8_t                     uuid_type;
    int                         blocking;
    app_fifo_t                  input_buffer;
    cipher_t                    cipher;
    GtoM_t                      GtoM;
    int                         MtoG;
    int                         insert_success;
};

/* Initialize the mOPE Service */
uint32_t ble_mops_init(ble_mops_t *p_mops, const ble_mops_init_t p_mops_init);

/* Function for handling the Application's BLE Stack events.  Handles
   all events from the BLE stack of interest to the mOPE Service */
void ble_mops_on_ble_evt(ble_mops_t *p_mops, uint16_t data);

/* Function for sending data to gateway/cloud over mOPE.  Will return 
   an ERROR_BUSY immediately if there is no room to enqueue data. 
   All other errors should be treated as a failure of the session */
uint32_t ble_mops_insert(ble_mops_t *p_mops, uint16_t data);

#endif // BLE_MOPS_H__



